# PyTest E2E Sample tests

End-to-end tests created to confirm, wherever implementation of the API:

    - [ex-book-admin-api-go](https://gitlab.com/ldath-core/workshops/core/ex-book-admin-api-go/-/blob/master/public/v1/openapi.yaml)
    - [ex-book-list-api-go](https://gitlab.com/ldath-core/workshops/core/ex-book-list-api-go/-/blob/master/public/v1/openapi.yaml) -> ToDo

## Project Structure

The test cases are implemented under tests/<api_name>(for instance book_admin_api) folders.
The support folder contains help python scripts, shared between test files

## Configuration

The End-to-end tests are using PyTest framework ([link](https://docs.pytest.org/en/7.2.x/)) to run tests on different API engine implementations. It is required to have installed Python 3.7+.
Before the first test configuration, it is required to install all python dependencies included in the **requirements.txt** file using cmd:

```sh
pip install -r requirements.txt
```

## Test Execution

Depending on the API which will be under test, each API section is tagged by markers. The markers are described in pytest.ini, and can be obtained by running:

```sh
pytest --host http://localhost --port 8883 --markers
```

To run all tests please use the command:

```sh
 pytest --host http://localhost --port 8883 --benchmark-histogram
```

Where
```
--`benchmark-histogram` will produce *.svg plot groups of the collected results. More about PyTest benchmarks can be obtained at the [link](https://pypi.org/project/pytest-benchmark/).
```

### book admin api

```
# All tests:
pytest --host http://localhost --port 8883 -m 'book_admin_api and not book_bff_api'
# Fast tests only:
pytest --host http://localhost --port 8883 -m 'book_admin_api and not book_admin_performance and not book_bff_api'
# Performance tests only:
pytest --host http://localhost --port 8883 -m 'book_admin_performance and not book_bff_api'
```

### book list api
```bash
# All tests:
pytest --host http://localhost --port 8882 -m 'book_list_api and not book_bff_api'
# Fast tests only:
pytest --host http://localhost --port 8882 -m 'book_list_api and not book_list_performance and not book_bff_api'
# Performance tests only:
pytest --host http://localhost --port 8882 -m 'book_list_performance and not book_bff_api'
```

## Development

The code style of the test is checked by the Flake8 python module((link)[https://pypi.org/project/flake8/]). After new test will be added to repository, the flake8 check should be executed using cmd:

```sh
flake8 --config .flake8 ./tests
flake8 --config .flake8 ./support
```

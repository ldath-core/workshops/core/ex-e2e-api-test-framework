"""

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

"""

import asyncio


def run_concurrent_requests(func, concurrent_limit, total, *args, **kwargs):
    '''Runs provided func in concurrent with limited number of concurrent executions
        Parameters:
            func: function that performs the requests
            concurrent_limit: max number of requests that will be active at one time
            total: total number of requests to execute
            *args: passed to the func during execution
            **kwargs: passed to the func during execution

    Supposed to be passed to benchmark.pedantic()
    Example usage:
        benchmark.pedantic(run_concurrent_requests(create_one_account, 30, 100), rounds=10)
    
    Note:
        Concurrent run enables possibility to run request handling in parallel way on server side.
    '''
    async def run_func_concurrently(func, concurrent_req_limit, total, args, kwargs):
        sem = asyncio.Semaphore(concurrent_req_limit)

        async def limit_concurrent_requests(func, args, kwargs):
            async with sem:
                await func(*args, **kwargs)

        tasks = [asyncio.ensure_future(limit_concurrent_requests(func, args, kwargs))
                 for i
                 in range(total)]
        await asyncio.gather(*tasks)

    def f():
        loop = asyncio.get_event_loop()
        loop.run_until_complete(run_func_concurrently(func, concurrent_limit, total, args, kwargs))
    return f

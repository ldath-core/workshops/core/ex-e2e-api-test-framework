"""

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

"""

import json
import requests
import unittest

from enum import Enum
from .http_support import HEADER_CONTENT_TYPE
from .http_support import STATUS_CODES

API_VERSION = 'v1'
API_BOOK_SUFFIX = f'book-list/{API_VERSION}/books'
API_ADMIN_SUFFIX = f'book-admin/{API_VERSION}/admins'
TEST_LOGIN_JSON = {
    "email": "atulodzi@gmail.com",
    "password": "NicePass4Me"
}


def getToken(api_url: str) -> str:
    """
        Proceed Login option to collect valid token

        :param str api_url: API url address
        :return: the string token presentation
    """
    response = requests.post(f"{api_url}/{API_VERSION}/login",
                             headers=HEADER_CONTENT_TYPE,
                             data=json.dumps(TEST_LOGIN_JSON),
                             timeout=2)

    response_json = json.loads(response.text)
    # Validate if the collecting the token is successful
    assert response.status_code == STATUS_CODES.RESPONSE_CODE_OK, response_json

    return response_json['content']['token']

def resetMockServer(unitTestClsObj:unittest.TestCase):
    """
        Reset Mock Services for Book and Admin List

        Parameters
        ----------
        unitTestClsObj
            unittest.TestCase class object, mostly the "self" from class which inherit from unittest.TestCase 
    """

    def sendResetRequest(serviceUrl:str):
        response = requests.post(f"{serviceUrl}/{API_VERSION}/mock-queue",
                             headers=HEADER_CONTENT_TYPE, data=json.dumps({"requestMethod":"RST_MOCK"}))
        response_json = json.loads(response.text)
        # In case of test failure, print response for feature investigation
        print(STATUS_CODES.RESPONSE_CODE_OK, response_json)

    sendResetRequest(unitTestClsObj.books_mock_host)
    sendResetRequest(unitTestClsObj.admin_mock_host)

class MockApiAdminReferenceTable(str, Enum):
    GET_HEALTH = "get_health"
    GET_ADMINS = "get_admins"
    POST_ADMINS = "post_admins"
    GET_ADMINS_ID = "get_admins_id"
    PUT_ADMINS_ID = "put_admins_id"
    DELETE_ADMINS_ID = "delete_admins_id"

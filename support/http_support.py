"""

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

"""


HEADER_CONTENT_TYPE = {'Content-Type': 'application/json'}


class STATUS_CODES:
    """
    HTTP Response status codes variables
    """
    RESPONSE_CODE_OK = 200
    RESPONSE_CODE_CREATED = 201
    RESPONSE_CODE_ACCEPTED = 202
    RESPONSE_BAD_REQUEST = 400
    RESPONSE_CODE_UNAUTHORIZED = 401
    RESPONSE_CODE_NOT_FOUND = 404
    RESPONSE_CODE_METHOD_NOT_ALLOWED = 405
    RESPONSE_NOT_ACCEPTABLE = 406
    RESPONSE_CODE_INTERNAL_SERVER_ERROR = 500
    RESPONSE_CODE_NOT_IMPLEMENTED = 501

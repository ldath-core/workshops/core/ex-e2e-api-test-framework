
def get_next_value():
    num = 0
    while True:
        yield num
        num += 1

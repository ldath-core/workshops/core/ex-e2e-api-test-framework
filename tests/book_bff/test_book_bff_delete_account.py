"""

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

"""


from dacite import from_dict
import json
import pytest
import requests
import sys

sys.path.insert(0,'..')
from support.http_support import STATUS_CODES
from support.http_support import HEADER_CONTENT_TYPE
from support.admin_account_support import AdminAccount
from tests.admin_book_api import test_delete_admin_data
from tests.book_bff.test_book_bff_common import BookBffAdminBookCommon


@pytest.mark.book_bff_delete_account
class BookBffAdminBookDelete(BookBffAdminBookCommon, test_delete_admin_data.AdminBookDelete):

    @pytest.mark.book_bff_mock_backend
    @pytest.mark.timeout(5)
    def test_delete_one_account_fail_as_header_does_not_contain_token(self):
        admin_data = self.getSampleAccountData()

        id = self.prepareEnv([admin_data], True)
        assert len(id) > 0

        response = requests.delete(f"{self.endpoint_url}/{self.getApiUrl()}/{id[0]}",
                                     headers=HEADER_CONTENT_TYPE)
        self.validateUnauthorized(response)


@pytest.mark.book_bff_delete_account
@pytest.mark.book_bff_mock_backend
class BookBffAdminBookDelete(BookBffAdminBookCommon):

    @pytest.mark.timeout(5)
    def test_delete_one_account_pass_as_header_contain_jwt_token(self):
        id = '6318f3d9c52dc30cc23bce80'

        response = requests.delete(f"{self.endpoint_url}/{self.getApiUrl()}/{id}",
                                     headers=self.getDeleteHeader())

        assert response.status_code == STATUS_CODES.RESPONSE_CODE_ACCEPTED

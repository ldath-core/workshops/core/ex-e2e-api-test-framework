"""

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

"""

import json
import pytest
import requests
import sys

sys.path.insert(0, '..')
from support.http_support import STATUS_CODES
from support.http_support import HEADER_CONTENT_TYPE
from tests.list_book_api import test_post_book_data
from tests.book_bff.test_book_bff_common import BookBffBookCommon


@pytest.mark.book_bff_add_book
class BookBffPost(BookBffBookCommon, test_post_book_data.BookPost):

    @pytest.mark.timeout(5)
    @pytest.mark.book_bff_mock_backend
    def test_add_book_fail_without_authorization_token_when_required_data_to_add_book_are_passed(self):
        book_data = self.getSampleBookData()

        self.prepareEnv([book_data], False)

        ##########################################################################
        # Add new book
        ##########################################################################
        response = requests.post(f"{self.endpoint_url}/{self.getApiUrl()}",
                                 headers=HEADER_CONTENT_TYPE,
                                 data=json.dumps(book_data))

        assert response.status_code == STATUS_CODES.RESPONSE_CODE_UNAUTHORIZED

"""

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

"""


import json
import pytest
import requests
import unittest
from support.book_bff import resetMockServer

import sys

sys.path.insert(0, '..')
from support.http_support import STATUS_CODES
from support.http_support import HEADER_CONTENT_TYPE
from support.book_bff import API_VERSION
from support.book_bff import TEST_LOGIN_JSON
from support.book_bff import MockApiAdminReferenceTable

@pytest.mark.usefixtures("host_and_port")
@pytest.mark.usefixtures("mock_host_and_port")
@pytest.mark.book_bff_api
@pytest.mark.book_bff_login
class BookBffApiLogin(unittest.TestCase):
    @pytest.mark.book_bff_mock_backend
    @pytest.mark.timeout(5)
    def test_login_process_fail_with_empty_json_login_content(self):
        loginJson = {}
        response = requests.post(f"{self.endpoint_url}/{API_VERSION}/login",
                                 headers=HEADER_CONTENT_TYPE,
                                 data=json.dumps(loginJson))

        assert response.status_code == STATUS_CODES.RESPONSE_BAD_REQUEST


    @pytest.mark.book_bff_mock_backend
    @pytest.mark.timeout(5)
    def test_login_process_fail_with_filled_only_email_json_login_content(self):
        loginJson = {'email': 'oz'}
        response = requests.post(f"{self.endpoint_url}/{API_VERSION}/login",
                                 headers=HEADER_CONTENT_TYPE,
                                 data=json.dumps(loginJson))

        assert response.status_code == STATUS_CODES.RESPONSE_BAD_REQUEST

    @pytest.mark.book_bff_mock_backend
    @pytest.mark.timeout(5)
    def test_login_process_fail_with_filled_only_password_json_login_content(self):
        loginJson = {'password': 'SoSecret'}
        response = requests.post(f"{self.endpoint_url}/{API_VERSION}/login",
                                 headers=HEADER_CONTENT_TYPE,
                                 data=json.dumps(loginJson))

        assert response.status_code == STATUS_CODES.RESPONSE_BAD_REQUEST

    @pytest.mark.book_bff_mock_backend
    @pytest.mark.timeout(5)
    def test_login_process_fail_with_filled_invalid_credential_data_in_json_login_content(self):
        loginJson = {
            'password': 'SoSecret',
            'email': 'Oz'
        }
        response = requests.post(f"{self.endpoint_url}/{API_VERSION}/login",
                                 headers=HEADER_CONTENT_TYPE,
                                 data=json.dumps(loginJson))

        assert response.status_code == STATUS_CODES.RESPONSE_BAD_REQUEST

    @pytest.mark.book_bff_mock_backend
    @pytest.mark.timeout(5)
    def test_login_process_success_with_filled_with_valid_credential_data_and_returnSecureToken_set_to_True_in_json_login_content(self):
        loginJson = TEST_LOGIN_JSON
        loginJson["returnSecureToken"] = True

        response = requests.post(f"{self.endpoint_url}/{API_VERSION}/login",
                                 headers=HEADER_CONTENT_TYPE,
                                 data=json.dumps(loginJson))
        assert response.status_code == STATUS_CODES.RESPONSE_CODE_OK
        response_json = json.loads(response.text)
        assert len(response_json['content']['token']) >  0

    @pytest.mark.book_bff_mock_backend
    @pytest.mark.timeout(5)
    def test_login_process_success_with_filled_with_valid_credential_data_and_returnSecureToken_set_to_False_in_json_login_content(self):
        loginJson = TEST_LOGIN_JSON
        loginJson["returnSecureToken"] = False
        response = requests.post(f"{self.endpoint_url}/{API_VERSION}/login",
                                 headers=HEADER_CONTENT_TYPE,
                                 data=json.dumps(loginJson))
        assert response.status_code == STATUS_CODES.RESPONSE_CODE_OK
        response_json = json.loads(response.text)
        assert len(response_json['content']['token']) >  0

    @pytest.mark.timeout(5)
    def test_login_process_success_with_filled_with_valid_credential_data_with_omitted_returnSecureToken_field_in_json_login_content(self):
        loginJson = TEST_LOGIN_JSON
        response = requests.post(f"{self.endpoint_url}/{API_VERSION}/login",
                                 headers=HEADER_CONTENT_TYPE,
                                 data=json.dumps(loginJson))
        assert response.status_code == STATUS_CODES.RESPONSE_CODE_OK
        response_json = json.loads(response.text)
        assert len(response_json['content']['token']) >  0


@pytest.mark.usefixtures("host_and_port")
@pytest.mark.usefixtures("mock_host_and_port")
@pytest.mark.book_bff_mock_backend
class BookBffApiLoginMockServer(unittest.TestCase):

    @pytest.mark.timeout(5)
    def test_login_process_fail_with_filled_with_valid_credential_data_with_omitted_returnSecureToken_field_in_json_login_content_when_backend_return_two_accounts_for_one_email(self):
        resetMockServer(self)

        json_data = {
            "requestMethod": MockApiAdminReferenceTable.GET_ADMINS,
            "data": {
                "status": 200,
                "message": "admins - skip: 0; limit: 2",
                "content": {
                    "count": 2,
                    "skip": 0,
                    "limit": 2,
                    "results": [
                        {
                            "id": "6318f3b8c52dc30cc23bce7f",
                            "email": "atulodzi@gmail.com",
                            "firstName": "Main",
                            "lastName": "Administrator",
                            "passwordHash": "$2a$14$HxREbiVuB190sx4pOsit4uOu0/Z9YUHRRGYbQaSWEmFNh/v5SnJzW",
                            "version": 1
                        },
                        {
                            "id": "6318f3b8c52dc30cc23bed7f",
                            "email": "atulodzi@gmail.com",
                            "firstName": "Main",
                            "lastName": "Administrator",
                            "passwordHash": "$2a$14$HxRebiVuB190sx4pOsit4uOu0/Z9YUHRRGYbQaSWEmFNh/v5SnJzW",
                            "version": 1
                        }
                    ]
                }
            }
        }

        response = requests.post(f"{self.admin_mock_host}/{API_VERSION}/mock-queue",
                                 headers=HEADER_CONTENT_TYPE, data=json.dumps(json_data))

        response = requests.post(f"{self.endpoint_url}/{API_VERSION}/login",
                                 headers=HEADER_CONTENT_TYPE,
                                 data=json.dumps(TEST_LOGIN_JSON))
        assert response.status_code == STATUS_CODES.RESPONSE_CODE_INTERNAL_SERVER_ERROR, response

    @pytest.mark.timeout(5)
    def test_login_process_fail_with_filled_with_valid_credential_data_with_omitted_returnSecureToken_field_in_json_login_content_when_backend_return_zero_accounts_for_defined_email(self):
        resetMockServer(self)

        json_data = {
            "requestMethod": MockApiAdminReferenceTable.GET_ADMINS,
            "data": {
                "status": 200,
                "message": "admins - skip: 0; limit: 2",
                "content": {
                    "count": 0,
                    "skip": 0,
                    "limit": 2,
                    "results": []
                }
            }
        }

        response = requests.post(f"{self.admin_mock_host}/{API_VERSION}/mock-queue",
                                 headers=HEADER_CONTENT_TYPE, data=json.dumps(json_data))

        response = requests.post(f"{self.endpoint_url}/{API_VERSION}/login",
                                 headers=HEADER_CONTENT_TYPE,
                                 data=json.dumps(TEST_LOGIN_JSON))
        assert response.status_code == STATUS_CODES.RESPONSE_BAD_REQUEST, response

    @pytest.mark.timeout(5)
    def test_login_process_fail_with_filled_with_valid_credential_data_with_omitted_returnSecureToken_field_in_json_login_content_when_backend_return_bad_admin_json_response_struct(self):
        resetMockServer(self)

        json_data = {
            "requestMethod": MockApiAdminReferenceTable.GET_ADMINS,
            "data": {
                "status": 200,
                "message": "admins - skip: 0; limit: 1",
                "content": {
                    "count": 1,
                    "skip": 0,
                    "limit": 2,
                    "results": [
                        {
                            "Id": "6318f3b8c52dc30cc23bce7f",
                            "E-mail": "atulodzi@gmail.com",
                            "Password_Hash": "$2a$14$HxREbiVuB190sx4pOsit4uOu0/Z9YUHRRGYbQaSWEmFNh/v5SnJzW",
                            "Version": 1
                        }
                    ]
                }
            }
        }

        response = requests.post(f"{self.admin_mock_host}/{API_VERSION}/mock-queue",
                                 headers=HEADER_CONTENT_TYPE, data=json.dumps(json_data))

        response = requests.post(f"{self.endpoint_url}/{API_VERSION}/login",
                                 headers=HEADER_CONTENT_TYPE,
                                 data=json.dumps(TEST_LOGIN_JSON))
        response_json = json.loads(response.text)
        assert response.status_code == STATUS_CODES.RESPONSE_BAD_REQUEST, response_json

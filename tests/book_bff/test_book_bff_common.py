"""

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

"""


import pytest
import json
import sys

sys.path.insert(0, '..')
from support.http_support import HEADER_CONTENT_TYPE
from support.http_support import STATUS_CODES
from support.book_bff import API_BOOK_SUFFIX
from support.book_bff import API_ADMIN_SUFFIX
from support.book_bff import TEST_LOGIN_JSON
from support.book_bff import getToken

from tests.list_book_api import test_book_list_common
from tests.admin_book_api import test_admin_common

@pytest.mark.book_bff_api
class BookBffBookCommon(test_book_list_common.BookListCommon):

    def setup_method(self, test_method):
        authorization  = { 'Authorization': getToken(f"{self.endpoint_url}") }
        authorization.update(HEADER_CONTENT_TYPE)
        self.autorization = authorization

    def getPostHeader(self):
        """
        Overwrite Post header as creating new admin data it should only be available for authorized users
        """
        return self.autorization
    
    def getDeleteHeader(self):
        """
        Overwrite Delete header as the delete admin account it should only be available for authorized users
        """
        return self.autorization

    def getPutHeader(self):
        """
        Overwrite Put header as the update addmin data it should only be available for authorized users
        """
        return self.autorization

    def getHeader(self):
        """
        Overwrite Get header as the get addmin data it should only be available for authorized users
        """
        return self.autorization

    def getApiUrl(self):
        """
        Enable overwrite and defining in child class after inheritance
        """
        return f"{API_BOOK_SUFFIX}"

    def avoidDeleting(self):
        """
        Disable deleting default account required to collect token
        """
        return TEST_LOGIN_JSON["email"]


@pytest.mark.book_bff_api
class BookBffAdminBookCommon(test_admin_common.AdminBookCommon):
    def setup_method(self, test_method):
        authorization  = { 'Authorization': getToken(f"{self.endpoint_url}") }
        authorization.update(HEADER_CONTENT_TYPE)
        self.autorization = authorization

    def getPostHeader(self):
        """
        Overwrite Post header as creating new admin data it should only be available for authorized users
        """
        return self.autorization

    def getDeleteHeader(self):
        """
        Overwrite Delete header as the delete admin account it should only be available for authorized users
        """
        return self.autorization

    def getPutHeader(self):
        """
        Overwrite Put header as the update addmin data it should only be available for authorized users
        """
        return self.autorization
    
    def getHeader(self):
        """
        Overwrite Get header as the get addmin data it should only be available for authorized users
        """
        return self.autorization

    def getDeleteHeader(self):
        """
        Overwrite delete header as the get addmin data it should only be available for authorized users
        """
        return self.autorization

    def getApiUrl(self):
        """
        Enable overwrite and defining in child class after inheritance
        """
        return f"{API_ADMIN_SUFFIX}"

    def avoidDeleting(self):
        """
        Disable deleting default account required to collect token
        """
        return TEST_LOGIN_JSON["email"]

    def validateUnauthorized(self, response) -> dict:
        """
        Validate status for Unauthorized responses

        Params
        --------
        response
            response cls obj
        
        Returns
        response_json
            dictionary format of the response
        -------

        """
        assert response.status_code == STATUS_CODES.RESPONSE_CODE_UNAUTHORIZED
        response_json = json.loads(response.text)
        assert "message" in response_json.keys()
        assert "errors" in response_json.keys()
        return response_json

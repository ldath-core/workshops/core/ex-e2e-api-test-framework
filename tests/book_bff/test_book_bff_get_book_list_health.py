"""

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

"""


import pytest
import requests
import sys
import unittest

sys.path.insert(0, '..')
from support.http_support import STATUS_CODES
from support.http_support import HEADER_CONTENT_TYPE
from support.book_bff import API_BOOK_SUFFIX

from tests.list_book_api import test_book_list_health
from tests.book_bff.test_book_bff_common import BookBffBookCommon


@pytest.mark.book_bff_book_list_backend_health
class BookBffListBookApiHealth(BookBffBookCommon, test_book_list_health.ListBookCommonApiHealth):

    def getApiUrl(self):
        """
        Enable overwrite and defining in child class after inheritance
        """
        return f"{API_BOOK_SUFFIX.replace('/books', '')}"

    @pytest.mark.timeout(5)
    @pytest.mark.book_bff_mock_backend
    def test_health_status_fail_without_authorization_token(self):
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}/health", headers=HEADER_CONTENT_TYPE)
        assert response.status_code == STATUS_CODES.RESPONSE_CODE_UNAUTHORIZED, f"{self.endpoint_url}/{self.getApiUrl()}/health"


@pytest.mark.book_bff_mock_backend
@pytest.mark.usefixtures("host_and_port")
class BookBffListBookApiHealth(BookBffBookCommon):
    def getApiUrl(self):
        """
        Enable overwrite and defining in child class after inheritance
        """
        return f"{API_BOOK_SUFFIX.replace('/books', '')}"

    @pytest.mark.timeout(5)
    def test_health_status_pass_with_authorization_token(self):

        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}/health", headers=self.getHeader())
        assert response.status_code == STATUS_CODES.RESPONSE_CODE_OK

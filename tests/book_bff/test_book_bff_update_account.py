"""

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

"""


import json
import pytest
import requests
import sys

sys.path.insert(0,'..')
from support.http_support import STATUS_CODES
from support.http_support import HEADER_CONTENT_TYPE
from support.book_bff import getToken
from support.book_bff import resetMockServer
from tests.admin_book_api import test_update_admin_data
from tests.book_bff.test_book_bff_common import BookBffAdminBookCommon


@pytest.mark.book_bff_update_account
class BookBffUpdateAccount(BookBffAdminBookCommon, test_update_admin_data.AdminBookUpdate):

    @pytest.mark.timeout(10)
    def test_update_first_name_in_one_account_fail_as_header_does_not_contain_token(self):
        admin_data = self.getSampleAccountData()
        
        id = self.prepareEnv([admin_data], False)

        #####################################################################
        # Update Name
        #####################################################################
        replace_name = { "firstName": "Marcus"}
        response = requests.put(f"{self.endpoint_url}/{self.getApiUrl()}/{id}",
                                headers=HEADER_CONTENT_TYPE,
                                data=json.dumps(replace_name))
        self.validateUnauthorized(response)


@pytest.mark.usefixtures("host_and_port")
@pytest.mark.usefixtures("mock_host_and_port")
@pytest.mark.book_bff_update_account
@pytest.mark.book_bff_mock_backend
class BookBffUpdateAccountMock(BookBffAdminBookCommon):

    @pytest.mark.timeout(10)
    def test_update_first_name_in_existing_account_fail_when_header_does_not_contain_token(self):
        resetMockServer(self)
        id = '6318f3d9c52dc30cc23bce80'

        #####################################################################
        # Try to Update Name without valid authentication code
        #####################################################################
        replace_name = { "firstName": "Marcus"}
        response = requests.put(f"{self.endpoint_url}/{self.getApiUrl()}/{id}",
                                headers=HEADER_CONTENT_TYPE,
                                data=json.dumps(replace_name))

        self.validateUnauthorized(response)
        
    @pytest.mark.timeout(10)
    def test_update_first_name_for_existing_account_pass_when_header_contains_token(self):
        resetMockServer(self)
        id = '6318f3d9c52dc30cc23bce80'

        #####################################################################
        # Try to Update Name with valid authentication code
        #####################################################################
        replace_name = { "firstName": "Marcus"}
        response = requests.put(f"{self.endpoint_url}/{self.getApiUrl()}/{id}",
                                headers=self.getPutHeader(),
                                data=json.dumps(replace_name))

        assert response.status_code == STATUS_CODES.RESPONSE_CODE_ACCEPTED

    @pytest.mark.timeout(10)
    def test_update_first_name_fail_when_id_is_invalid_and_header_contains_jwt_token(self):
        resetMockServer(self)
        id = 'zumzum'

        #####################################################################
        # Try to Update Name with valid authentication code
        #####################################################################
        replace_name = { "firstName": "Marcus"}
        response = requests.put(f"{self.endpoint_url}/{self.getApiUrl()}/{id}",
                                headers=self.getPutHeader(),
                                data=json.dumps(replace_name))

        assert response.status_code == STATUS_CODES.RESPONSE_BAD_REQUEST

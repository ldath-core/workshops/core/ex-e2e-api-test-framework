"""

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

"""


import json
import pytest
import requests
import sys
import unittest

sys.path.insert(0, '..')
from support.http_support import STATUS_CODES
from support.http_support import HEADER_CONTENT_TYPE
from support.book_bff import API_VERSION
from support.book_bff import getToken


@pytest.mark.usefixtures("host_and_port")
@pytest.mark.book_bff_api
@pytest.mark.book_bff_protected
@pytest.mark.book_bff_mock_backend
class BookBffProtected(unittest.TestCase):
    """
    Confirm, if the token passed to book-bff is still valid
    """

    @pytest.mark.timeout(5)
    def test_token_validation_process_fail_when_header_does_not_contain_authorized_param(self):
        response = requests.get(f"{self.endpoint_url}/{API_VERSION}/protected",
                                 headers=HEADER_CONTENT_TYPE)

        assert response.status_code == STATUS_CODES.RESPONSE_CODE_UNAUTHORIZED

    @pytest.mark.timeout(5)
    def test_token_validation_process_fail_when_header_contains_invalid_token(self):
        token = getToken(f"{self.endpoint_url}")
        token = token[-1::]  # Revert token to make it invalid

        authorization  = { 'Authorization': token }
        authorization.update(HEADER_CONTENT_TYPE)
        response = requests.get(f"{self.endpoint_url}/{API_VERSION}/protected",
                                 headers=authorization)

        assert response.status_code == STATUS_CODES.RESPONSE_CODE_UNAUTHORIZED

    @pytest.mark.timeout(5)
    def test_token_validation_process_fail_when_header_contain_token_with_spaces(self):
        token = getToken(f"{self.endpoint_url}")

        authorization  = { 'Authorization': f"Barer {token[:4]} {token[4:]}" }
        authorization.update(HEADER_CONTENT_TYPE)
        response = requests.get(f"{self.endpoint_url}/{API_VERSION}/protected",
                                 headers=authorization)

        assert response.status_code == STATUS_CODES.RESPONSE_BAD_REQUEST

    @pytest.mark.timeout(5)
    def test_token_validation_process_pass_when_header_contain_valid_authorized_param_without_jwt_prefix(self):
        authorization  = { 'Authorization': getToken(f"{self.endpoint_url}") }
        authorization.update(HEADER_CONTENT_TYPE)
        response = requests.get(f"{self.endpoint_url}/{API_VERSION}/protected",
                                 headers=authorization)

        assert response.status_code == STATUS_CODES.RESPONSE_CODE_OK
        response_json = json.loads(response.text)

        # Validate if all fields are available in response body
        assert "message" in response_json.keys()
        assert "content" in response_json.keys()

    @pytest.mark.timeout(5)
    @pytest.mark.book_bff_mock_backend
    def test_token_validation_process_pass_when_header_contain_valid_authorized_param_with_barer_jwt_prefix(self):
        authorization  = { 'Authorization': f"Barer {getToken(self.endpoint_url)}" }
        authorization.update(HEADER_CONTENT_TYPE)
        response = requests.get(f"{self.endpoint_url}/{API_VERSION}/protected",
                                 headers=authorization)

        assert response.status_code == STATUS_CODES.RESPONSE_CODE_OK
        response_json = json.loads(response.text)

        # Validate if all fields are available in response body
        assert "message" in response_json.keys()
        assert "content" in response_json.keys()

    @pytest.mark.timeout(5)
    def test_token_validation_process_fail_as_token_is_no_longer_valid(self):
        # Token generated 16:37 June 2000
        token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImF0dWxvZHppQGdtYWlsLmNvbSIsImV4cCI6OTYwMjU5MDIyLCJpc3MiOiJjb3Vyc2UifQ.0NfGTD26gcZVi2OCu8IY0Ar7kyv69S-e1CGswfZB9NQ"
        authorization  = { 'Authorization': f"Barer {token}" }
        authorization.update(HEADER_CONTENT_TYPE)
        response = requests.get(f"{self.endpoint_url}/{API_VERSION}/protected",
                                 headers=authorization)

        response_json = json.loads(response.text)
        assert response.status_code == STATUS_CODES.RESPONSE_CODE_UNAUTHORIZED, response_json

"""

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

"""


import json
import pytest
import requests
import sys

sys.path.insert(0,'..')
from support.http_support import STATUS_CODES
from support.http_support import HEADER_CONTENT_TYPE
#from tests.admin_book_api.test_post_admin_data import AdminBookPost
from tests.admin_book_api import test_post_admin_data
from tests.book_bff.test_book_bff_common import BookBffAdminBookCommon


@pytest.mark.book_bff_create_admin
@pytest.mark.book_bff_api
class BookBffAdminBookPost(BookBffAdminBookCommon, test_post_admin_data.AdminBookPost):

    @pytest.mark.timeout(5)
    def test_post_one_account_data_with_all_fields_fail_as_token_is_not_passed_in_header(self):
        admin_data = self.getSampleAccountData()

        self.prepareEnv([admin_data], False)

        ##########################################################################
        # Add new account
        ##########################################################################
        response = requests.post(f"{self.endpoint_url}/{self.getApiUrl()}",
                                 headers=HEADER_CONTENT_TYPE,
                                 data=json.dumps(admin_data))

        self.validateUnauthorized(response)


@pytest.mark.usefixtures("host_and_port")
@pytest.mark.book_bff_mock_backend
class BookBffCreateAccountMock(BookBffAdminBookCommon):

    @pytest.mark.timeout(10)
    def test_create_account_with_valid_data(self):
        admin_data = {
            "email": "zone@example.com",
            "firstName": "New",
            "lastName": "Person",
            "password": "oz",
            "version": 1,
        }

        self.prepareEnv([admin_data], False)

        ##########################################################################
        # Add new account
        ##########################################################################
        response = requests.post(f"{self.endpoint_url}/{self.getApiUrl()}",
                                 headers=self.getPostHeader(),
                                 data=json.dumps(admin_data))

        assert response.status_code == STATUS_CODES.RESPONSE_CODE_CREATED
        responseJson = json.loads(response.text)
        del admin_data['password']
        admin_data['passwordHash'] = "$2a$14$jOhB41Icyz0C6vCRT3xnq.shdvK0MgeKePPiJvE/15h9LPqSDX9ze"
        admin_data['id'] = "6318f3d9c52dc30cc23bce80"
        assert responseJson['content'] == admin_data

    @pytest.mark.timeout(10)
    def test_create_account_with_valid_data_and_wait_for_failure_as_account_already_exists(self):
        admin_data = self.getSampleAccountData() 
        admin_data["email"] = "already_exists@gmail.com"

        ##########################################################################
        # Add new account
        ##########################################################################
        response = requests.post(f"{self.endpoint_url}/{self.getApiUrl()}",
                                 headers=self.getPostHeader(),
                                 data=json.dumps(admin_data))

        assert response.status_code == STATUS_CODES.RESPONSE_NOT_ACCEPTABLE

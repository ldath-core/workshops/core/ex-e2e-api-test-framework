"""

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

"""


import json
import pytest
import requests
import sys

sys.path.insert(0, '..')
from support.http_support import STATUS_CODES
from support.http_support import HEADER_CONTENT_TYPE
from support.book_bff import API_BOOK_SUFFIX

from tests.list_book_api import test_update_book_data
from tests.book_bff.test_book_bff_common import BookBffBookCommon


@pytest.mark.book_bff_update_book
class BookBffUpdateBook(BookBffBookCommon, test_update_book_data.BookUpdate):

    @pytest.mark.timeout(5)
    @pytest.mark.book_bff_mock_backend
    def test_update__of_the_title_in_one_book_fail_without_authorization(self):
        book_data = self.getSampleBookData()

        id = self.prepareEnv([book_data], True)

        #####################################################################
        # Update title
        #####################################################################
        replace_title = {"title": "Replaced"}
        response = requests.put(f"{self.endpoint_url}/{API_BOOK_SUFFIX}/{id[-1]}",
                                headers=HEADER_CONTENT_TYPE,
                                data=json.dumps(replace_title))

        assert response.status_code == STATUS_CODES.RESPONSE_CODE_UNAUTHORIZED

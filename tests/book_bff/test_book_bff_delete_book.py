"""

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

"""


from dacite import from_dict
import json
import pytest
import requests
import sys

sys.path.insert(0, '..')
from support.http_support import STATUS_CODES
from support.http_support import HEADER_CONTENT_TYPE
from support.book_list_support import Book
from tests.list_book_api import test_delete_book_data
from tests.book_bff.test_book_bff_common import BookBffBookCommon


@pytest.mark.book_bff_delete_book
class BookBffDeleteBook(BookBffBookCommon, test_delete_book_data.BookDelete):

    @pytest.mark.timeout(5)
    @pytest.mark.book_bff_mock_backend
    def test_delete_one_book_fail_if_user_does_not_pass_token(self):
        book_data = self.getSampleBookData()
        id = self.prepareEnv([book_data], True)

        response = requests.delete(f"{self.endpoint_url}/{self.getApiUrl()}/{id[-1]}", headers=HEADER_CONTENT_TYPE)

        assert response.status_code == STATUS_CODES.RESPONSE_CODE_UNAUTHORIZED

@pytest.mark.book_bff_mock_backend
class BookBffDeleteBookMock(BookBffBookCommon):
    @pytest.mark.timeout(5)
    def test_delete_one_book_pass_if_user_pass_token_in_header(self):
        id = "4"
        response = requests.delete(f"{self.endpoint_url}/{self.getApiUrl()}/{id}", headers=self.getDeleteHeader())

        assert response.status_code == STATUS_CODES.RESPONSE_CODE_OK

"""

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

"""


import json
import pytest
import requests
import sys

sys.path.insert(0,'..')
from support.http_support import HEADER_CONTENT_TYPE
from support.book_bff import API_ADMIN_SUFFIX
from tests.admin_book_api.test_health import AdminBookApiHealth
from tests.book_bff.test_book_bff_common import BookBffAdminBookCommon


@pytest.mark.book_bff_account_backend_health
class BookBffAdminBookApiHealth(BookBffAdminBookCommon, AdminBookApiHealth):

    def getApiUrl(self):
        """
        Enable overwrite and defining in child class after inheritance
        To point to correct endpoint
        """
        return f"{API_ADMIN_SUFFIX.replace('/admins', '')}"

    @pytest.mark.timeout(5)
    def test_health_status_fail_if_token_is_missed_in_header(self):
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}/health", headers=HEADER_CONTENT_TYPE)
        response_json = json.loads(response.text)
        assert "message" in response_json.keys(), response_json
        assert "errors" in response_json.keys(), response_json

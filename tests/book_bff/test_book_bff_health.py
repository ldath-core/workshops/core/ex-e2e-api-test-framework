"""

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

"""


import json
import pytest
import requests
import unittest
import sys

sys.path.insert(0, '..')
from support.http_support import STATUS_CODES
from support.http_support import HEADER_CONTENT_TYPE
from support.book_bff import API_VERSION
from support.book_bff import resetMockServer
from support.book_bff import MockApiAdminReferenceTable


@pytest.mark.usefixtures("host_and_port")
@pytest.mark.book_bff_api
@pytest.mark.book_bff_health
@pytest.mark.book_bff_mock_backend
class BookBffApiHealth(unittest.TestCase):
    @pytest.mark.timeout(5)
    def test_health_status(self):
        response = requests.get(f"{self.endpoint_url}/{API_VERSION}/health", headers=HEADER_CONTENT_TYPE)
        response_json = json.loads(response.text)
        assert response.status_code == STATUS_CODES.RESPONSE_CODE_OK
        assert response_json['content']['alive'] == True
        assert response_json['content']['redis'] == True
        assert response_json['content']['servicesHealth']['bookList'] == True
        assert response_json['content']['servicesHealth']['bookAdmin'] == True

@pytest.mark.usefixtures("mock_host_and_port")
@pytest.mark.usefixtures("host_and_port")
@pytest.mark.book_bff_mock_backend
class BookBffApiHealthMocked(unittest.TestCase):

    @pytest.mark.timeout(5)
    def test_health_return_False_for_Admin_when_output_format_is_incorrect(self):
        resetMockServer(self)
        json_data = {
            "requestMethod": MockApiAdminReferenceTable.GET_HEALTH,
            "data": { "status": 200, "error": "Bad data"}
        }

        response_post = requests.post(f"{self.admin_mock_host}/{API_VERSION}/mock-queue",
                                      headers=HEADER_CONTENT_TYPE, data=json.dumps(json_data))
        print(response_post)
        response = requests.get(f"{self.endpoint_url}/{API_VERSION}/health", headers=HEADER_CONTENT_TYPE)
        response_json = json.loads(response.text)
        assert response.status_code == STATUS_CODES.RESPONSE_CODE_OK
        assert response_json['content']['alive'] == True, response_json
        assert response_json['content']['redis'] == True, response_json
        assert response_json['content']['servicesHealth']['bookAdmin'] == False, response_json
        assert response_json['content']['servicesHealth']['bookList'] == True, response_json

    @pytest.mark.timeout(5)
    def test_health_return_False_for_Books_when_output_format_is_incorrect(self):
        resetMockServer(self)

        json_data = {
            "requestMethod": MockApiAdminReferenceTable.GET_HEALTH,
            "data": { "status": 200, "error": "Bad data"}
        }
        response = requests.post(f"{self.books_mock_host}/{API_VERSION}/mock-queue",
                                 headers=HEADER_CONTENT_TYPE, data=json.dumps(json_data))
        response = requests.get(f"{self.endpoint_url}/{API_VERSION}/health", headers=HEADER_CONTENT_TYPE)
        response_json = json.loads(response.text)
        assert response.status_code == STATUS_CODES.RESPONSE_CODE_OK
        assert response_json['content']['alive'] == True, response_json
        assert response_json['content']['redis'] == True, response_json
        assert response_json['content']['servicesHealth']['bookList'] == False, response_json
        assert response_json['content']['servicesHealth']['bookAdmin'] == True, response_json

    @pytest.mark.timeout(5)
    def test_health_return_False_for_Books_and_admin_when_output_format_is_incorrect(self):
        resetMockServer(self)

        json_data = {
            "requestMethod": MockApiAdminReferenceTable.GET_HEALTH,
            "data": { "status": 200, "error": "Bad data"}
        }

        response = requests.post(f"{self.books_mock_host}/{API_VERSION}/mock-queue",
                                 headers=HEADER_CONTENT_TYPE, data=json.dumps(json_data))
        response = requests.post(f"{self.admin_mock_host}/{API_VERSION}/mock-queue",
                                 headers=HEADER_CONTENT_TYPE, data=json.dumps(json_data))
        response = requests.get(f"{self.endpoint_url}/{API_VERSION}/health", headers=HEADER_CONTENT_TYPE)
        response_json = json.loads(response.text)
        assert response.status_code == STATUS_CODES.RESPONSE_CODE_OK
        assert response_json['content']['alive'] == True, response_json
        assert response_json['content']['redis'] == True, response_json
        assert response_json['content']['servicesHealth']['bookList'] == False, response_json
        assert response_json['content']['servicesHealth']['bookAdmin'] == False, response_json

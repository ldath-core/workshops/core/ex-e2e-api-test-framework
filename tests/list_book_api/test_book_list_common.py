"""

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

"""

import json
import pytest
import requests
import sys
import unittest


sys.path.insert(0, '..')
from support.http_support import HEADER_CONTENT_TYPE
from support.book_list_support import API_SUFFIX, API_HEALTH_SUFFIX
from support.http_support import STATUS_CODES

@pytest.mark.usefixtures("host_and_port")
class BookListCommon(unittest.TestCase):

    @pytest.fixture(autouse=True)
    def setupBenchmark(self, benchmark):
        self.benchmark = benchmark

    def getApiUrl(self):
        """
        Enable overwrite and defining in child class after inheritance
        """
        return API_SUFFIX

    def getApiHealthUrl(self):
        """
        Enable overwrite and defining in child class after inheritance
        """
        return API_HEALTH_SUFFIX

    def getHeader(self):
        """
        Enable overwrite and defining in child class after inheritance
        """
        return HEADER_CONTENT_TYPE

    def getPostHeader(self):
        """
        Enable overwrite and defining in child class after inheritance
        """
        return self.getHeader()

    def getDeleteHeader(self):
        """
        Enable overwrite and defining in child class after inheritance
        """
        return self.getHeader()

    def getPutHeader(self):
        """
        Enable overwrite and defining in child class after inheritance
        """
        return self.getHeader()

    def createBookItem(self, book_data:dict)->int:
        """
        Create new book

        Parameters
        ----------
            self: clsobj
                unittest cls instance object
            book_data: dict
                dictionary which store information required to create new book

        Returns
        ----------
            str
                id of newly created book
        """
        response = requests.post(f"{self.endpoint_url}/{self.getApiUrl()}",
                                 headers=self.getPostHeader(),
                                 data=json.dumps(book_data))
        response_json = json.loads(response.text)
        assert response.status_code == STATUS_CODES.RESPONSE_CODE_CREATED
        assert isinstance(response_json['content']['id'], int)

        return response_json['content']['id']

    def prepareEnv(self, book_data: list, create_book: bool)->list:
        """
        Create book if not exists, if exists, delete it and recreate it

        Parameters
        ----------
            self: clsobj
                unittest cls instance object
            book_data: list
                list of book items described by dictionary which store information required to create new book
            create_book: bool
                True -> create book
                False -> omit creating new book
        
        Returns
        ----------
            list
                list of id of newly created books. In case if create_book is set to False, return None
        """
        ret_value = None
        #####################################################################
        # Check if book does not exists, assumed no more than 100k books can
        # be in the DB. Tests assume this removes everything
        #####################################################################
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?limit=100000",
                                headers=self.getHeader())
        response_json = json.loads(response.text)

        if response.status_code == STATUS_CODES.RESPONSE_CODE_OK and\
           response_json['content']['count'] >= 1:
            for i in range(0, len(response_json['content']['results'])):
                response = requests.delete(f"{self.endpoint_url}/{self.getApiUrl()}/{response_json['content']['results'][i]['id']}",
                                           headers=self.getDeleteHeader())

                assert response.status_code == STATUS_CODES.RESPONSE_CODE_ACCEPTED

        if create_book:
            assert isinstance(book_data, list)
            ret_value = []
            for i in range(0, len(book_data)):
                ret_value.append(self.createBookItem(book_data[i]))

        return ret_value
    

    def getSampleBookData(self) -> dict:
        title = "Matrix"
        author = "Thomas Anderson"
        year = "2023"
        description = "test"
        img_type = "url"
        img_url = "http://test.com"
        book_data = {
            "title": title,
            "author": author,
            "year": year,
            "description": description,
            "image": {
                "type": img_type,
                "data": img_url}}
        return book_data

    def validateResponsBaseValues(self, response, *,
                                  status_code:STATUS_CODES = STATUS_CODES.RESPONSE_CODE_OK,
                                  skip:int = 0,
                                  limit:int = 10,
                                  count:int = 0 ):

        response_json = json.loads(response.text)
        assert response.status_code == status_code
        assert response_json['content']['skip'] == skip
        assert response_json['content']['limit'] == limit
        assert response_json['content']['count'] >= count
        if response_json['content']['count'] > response_json['content']['skip']:
            assert isinstance(response_json['content']['results'], list)

        return response_json

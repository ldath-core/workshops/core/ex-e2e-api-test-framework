"""

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

"""


from dacite import from_dict
import json
import pytest
import requests
import sys

from tests.list_book_api.test_book_list_common import BookListCommon

sys.path.insert(0, '..')
from support.http_support import STATUS_CODES
from support.book_list_support import Book


@pytest.mark.book_list_api
@pytest.mark.delete_book
class BookDelete(BookListCommon):
    @pytest.mark.timeout(5)
    def test_delete_one_book(self):
        book_data = self.getSampleBookData()

        id = self.prepareEnv([book_data], True)

        response = requests.delete(f"{self.endpoint_url}/{self.getApiUrl()}/{id[-1]}", headers=self.getDeleteHeader())

        assert response.status_code == STATUS_CODES.RESPONSE_CODE_ACCEPTED

    @pytest.mark.timeout(5)
    def test_delete_non_existing_book(self):

        id = "dumdum"
        # Ensure that book will not exist
        requests.delete(f"{self.endpoint_url}/{self.getApiUrl()}/{id}", headers=self.getDeleteHeader())

        # Try to delete not existing book
        response = requests.delete(f"{self.endpoint_url}/{self.getApiUrl()}/{id}", headers=self.getDeleteHeader())

        assert response.status_code != STATUS_CODES.RESPONSE_CODE_ACCEPTED

    @pytest.mark.book_list_performance
    def test_delete_100_books_sequential(self):
        test_round = 100
        book_data = [self.getSampleBookData() for i in range(test_round)]

        self.id_list = self.prepareEnv(book_data, True)
        assert isinstance(self.id_list, list)

        def delete_one_book():
            #  Collect id of existing book
            id = self.id_list[-1]
            self.id_list.pop()

            assert id != 0
            response = requests.delete(f"{self.endpoint_url}/{self.getApiUrl()}/{id}",
                                       headers=self.getDeleteHeader())
            assert response.status_code == STATUS_CODES.RESPONSE_CODE_ACCEPTED

        self.benchmark.pedantic(delete_one_book,  rounds=test_round)


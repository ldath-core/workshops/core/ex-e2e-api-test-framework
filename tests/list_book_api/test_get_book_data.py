"""

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

"""


from dacite import from_dict
import json
import pytest
import random
import requests
import sys

from tests.list_book_api.test_book_list_common import BookListCommon

sys.path.insert(0, '..')
from support.http_support import STATUS_CODES
from support.http_support import HEADER_CONTENT_TYPE
from support.book_list_support import Book


@pytest.mark.book_list_api
@pytest.mark.get_book
class BookGet(BookListCommon):
    @pytest.mark.timeout(5)
    def test_get_books_data(self):
        #####################################################################
        # Check inital value of content without defined query
        #####################################################################
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}", headers=self.getHeader())
        self.validateResponsBaseValues(response)

    def test_check_books_data(self):
        self.prepareEnv([self.getSampleBookData()], True)

        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}", headers=self.getHeader())
        response_json = self.validateResponsBaseValues(response)

        if response_json['content']['count'] > 0:
            if response_json['content']['count'] > 10:
                count = 10
            else:
                count = response_json['content']['count']

            for index in range(0, count):
                book = from_dict(data_class=Book, data=response_json['content']['results'][index])

                assert isinstance(book.id, int)
                assert len(book.title) > 0
                assert len(book.author) > 0
                assert len(book.year) >= 0

    @pytest.mark.get_book_query
    def test_check_books_data_with_limit_query_set_to_zero_return_one(self):
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?limit=0", headers=self.getHeader())
        assert response.status_code != STATUS_CODES.RESPONSE_CODE_OK

    @pytest.mark.get_book_query
    def test_check_books_data_with_limit_query_set_to_3(self):
        limit = 3

        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?limit={limit}", headers=self.getHeader())
        response_json = self.validateResponsBaseValues(response, limit=limit)

        #####################################################################
        # Check wherever the results is not null, as we expect that db is
        # filled with some data
        #####################################################################
        if response_json['content']['results']:
            assert len(response_json['content']['results']) >= 0
    
    @pytest.mark.get_book_query
    def test_check_books_data_with_skip_query_set_to_zero(self):
        skip = 0
        
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?skip={skip}", headers=self.getHeader())
        response_json = self.validateResponsBaseValues(response, skip=skip)

        #####################################################################
        # Check wherever the results is not null, as we expect that db is
        # filled with some data
        #####################################################################
        if response_json['content']['results']:
            assert len(response_json['content']['results']) >= 0
    
    @pytest.mark.get_book_query
    def test_check_books_data_with_skip_query_set_to_3(self):
        self.prepareEnv([self.getSampleBookData() for x in range(5)],True)
        skip = 3
        
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?skip={skip}", headers=self.getHeader())
        response_json = self.validateResponsBaseValues(response, skip=skip)

        #####################################################################
        # Check wherever the results is not null, as we expect that db is
        # filled with some data
        #####################################################################
        if response_json['content']['results']:
            assert len(response_json['content']['results']) >= 0

    @pytest.mark.get_book_query
    def test_check_books_data_with_skip_query_set_to_3_and_limit_set_to_0(self):
        skip = 3
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?skip={skip}&limit=0", headers=self.getHeader())
        assert response.status_code != STATUS_CODES.RESPONSE_CODE_OK

        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?limit=0&skip={skip}", headers=self.getHeader())
        assert response.status_code != STATUS_CODES.RESPONSE_CODE_OK

    @pytest.mark.get_book_query
    def test_check_books_data_with_skip_query_set_to_3_and_limit_set_to_3(self):
        self.prepareEnv([self.getSampleBookData() for x in range(5)],True)
        skip = 3
        limit = 3

        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?skip={skip}&limit={limit}", headers=self.getHeader())
        response_json = self.validateResponsBaseValues(response, skip=skip, limit=limit)

        #####################################################################
        # Check wherever the results is not null, as we expect that db is
        # filled with some data
        #####################################################################
        if response_json['content']['results']:
            assert len(response_json['content']['results']) >= 0

        #####################################################################
        # Invert Query
        #####################################################################
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?limit={limit}&skip={skip}", headers=self.getHeader())
        response_json = self.validateResponsBaseValues(response, skip=skip, limit=limit)

        #####################################################################
        # Check wherever the results is not null, as we expect that db is
        # filled with some data
        #####################################################################
        if response_json['content']['results']:
            assert len(response_json['content']['results']) >= 0

    @pytest.mark.get_book_query
    def test_check_books_data_with_skip_and_limit_query_set_to_random_values(self):
        skip = random.randint(0, 100)
        limit = random.randint(1, 100)
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?skip={skip}&limit={limit}", headers=self.getHeader())
        response_json = self.validateResponsBaseValues(response, skip=skip, limit=limit)

        #####################################################################
        # Check wherever the results is not null, as we expect that db is
        # filled with some data
        #####################################################################
        if response_json['content']['results']:
            assert len(response_json['content']['results']) >= 0

        #####################################################################
        # Invert Query
        #####################################################################
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?limit={limit}&skip={skip}", headers=self.getHeader())
        response_json = self.validateResponsBaseValues(response, skip=skip, limit=limit)

        #####################################################################
        # Check wherever the results is not null, as we expect that db is
        # filled with some data
        #####################################################################
        if response_json['content']['results']:
            assert len(response_json['content']['results']) >= 0

    @pytest.mark.get_book_query
    def test_check_books_data_basing_on_nonexisting_id(self):
        self.prepareEnv([], False)

        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}/999", headers=self.getHeader())
        print(response.text)
        assert response.status_code != STATUS_CODES.RESPONSE_CODE_OK

        response_json = json.loads(response.text)
        assert isinstance(response_json['message'], str)
        assert isinstance(response_json['status'], int)

    @pytest.mark.get_book_query
    def test_check_books_data_basing_on_existing_id(self):
        book_data = self.getSampleBookData()
        id = self.prepareEnv([book_data], True)

        #####################################################################
        # Confirm that data exists in db and is correctly return by get
        #####################################################################
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}/{id[-1]}", headers=self.getHeader())
        response_json = json.loads(response.text)
        assert response.status_code == STATUS_CODES.RESPONSE_CODE_OK

        book = from_dict(data_class=Book, data=response_json['content'])

        found_book = False
        if book_data['title'] == book.title and\
           book_data['author'] == book.author and\
           book_data['year'] == book.year and\
           book_data['description'] == book.description and\
           book_data['image']['type'] == book.image.type and\
           book_data['image']['data'] == book.image.data:
            found_book = True

        assert found_book

    @pytest.mark.book_list_performance
    def test_get_100_books_sequential(self):
        books_amount = 100
        book_data = [self.getSampleBookData() for i in range(0, books_amount)]
        self.prepareEnv(book_data, True)

        def get_one_book():

            for i in range(0, 100):
                if i == 0:
                    response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?limit=1", headers=self.getHeader())
                else:
                    response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?skip={i}&limit=1", headers=self.getHeader())
                response_json = self.validateResponsBaseValues(response, skip=i, limit=1, count=100)

                assert len(response_json['content']['results']) == 1  # to be fixed after discussion

        self.benchmark.pedantic(get_one_book, rounds=10)

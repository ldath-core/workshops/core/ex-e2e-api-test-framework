"""

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

"""


from dacite import from_dict
import json
import pytest
import random
import requests
import string
import sys

from tests.list_book_api.test_book_list_common import BookListCommon

sys.path.insert(0, '..')
from support.http_support import STATUS_CODES
from support.book_list_support import Book


@pytest.mark.book_list_api
@pytest.mark.update_book
class BookUpdate(BookListCommon):

    def validateData(self, book_id: str, reference_data: dict):
        #####################################################################
        # Confirm that new book exists, and
        # it can be obtained by get request
        #####################################################################
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}/{book_id}",
                                 headers=self.getHeader())
        
        assert response.status_code == STATUS_CODES.RESPONSE_CODE_OK
        response_json = json.loads(response.text)
        book = from_dict(data_class=Book, data=response_json['content'])
        assert isinstance(book.id, int)

        assert reference_data['title'] == book.title
        assert reference_data['author'] == book.author
        assert reference_data['year'] == book.year
        if 'description' in reference_data:
            assert reference_data['description'] == book.description
        if 'image' in reference_data:
            if 'url' in reference_data['image']:
                assert reference_data['image']['url'] == book.image.url
            if 'base64' in reference_data['image']:
                assert reference_data['image']['base64'] == book.image.base64

    def get_random_string(self, length: int = 5):
        #####################################################################
        # Generate random String
        #####################################################################
        letters = string.ascii_lowercase
        result_str = ''.join(random.choice(letters) for i in range(length))
        return result_str

    @pytest.mark.timeout(5)
    def test_update_title_in_one_book(self):
        book_data = self.getSampleBookData()
        
        id = self.prepareEnv([book_data], True)

        #####################################################################
        # Update title
        #####################################################################
        replace_title = {"title": "Replaced"}
        response = requests.put(f"{self.endpoint_url}/{self.getApiUrl()}/{id[-1]}",
                                headers=self.getPutHeader(),
                                data=json.dumps(replace_title))

        assert response.status_code == STATUS_CODES.RESPONSE_CODE_ACCEPTED
        response_json = json.loads(response.text)
        assert response_json['content']['title'] == replace_title['title']
        
        ##########################################################################
        # Confirm that new book exists, and
        # it can be obtained by get request
        ##########################################################################
        book_data['title'] = replace_title['title']
        self.validateData(
            book_id=id[0],
            reference_data=book_data)

    @pytest.mark.timeout(5)
    def test_update_author_in_one_book(self):
        book_data = self.getSampleBookData()

        id = self.prepareEnv([book_data], True)

        #####################################################################
        # Update author
        #####################################################################
        replace_author = {"author": "Replaced"}
        response = requests.put(f"{self.endpoint_url}/{self.getApiUrl()}/{id[-1]}",
                                headers=self.getPutHeader(),
                                data=json.dumps(replace_author))

        assert response.status_code == STATUS_CODES.RESPONSE_CODE_ACCEPTED
        response_json = json.loads(response.text)
        assert response_json['content']['author'] == replace_author['author']
        
        ##########################################################################
        # Confirm that new book exists, and
        # it can be obtained by get request
        ##########################################################################
        book_data["author"] =  replace_author["author"]
        self.validateData(
            book_id=id[0],
            reference_data=book_data)

    @pytest.mark.timeout(5)
    def test_update_year_in_one_book(self):
        book_data = self.getSampleBookData()

        id = self.prepareEnv([book_data], True)

        #####################################################################
        # Update year
        #####################################################################
        replace_year = {"year": "2022"}
        response = requests.put(f"{self.endpoint_url}/{self.getApiUrl()}/{id[-1]}",
                                headers=self.getPutHeader(),
                                data=json.dumps(replace_year))

        assert response.status_code == STATUS_CODES.RESPONSE_CODE_ACCEPTED
        response_json = json.loads(response.text)
        assert response_json['content']['year'] == replace_year['year']

        ##########################################################################
        # Confirm that new book exists, and
        # it can be obtained by get request
        ##########################################################################
        book_data['year'] = replace_year['year']
        self.validateData(
            book_id=id[0],
            reference_data=book_data)

    @pytest.mark.timeout(5)
    def test_update_description_in_one_book(self):
        book_data = self.getSampleBookData()

        id = self.prepareEnv([book_data], True)

        #####################################################################
        # Update year
        #####################################################################
        replace_description = {"description": "Replaced"}
        response = requests.put(f"{self.endpoint_url}/{self.getApiUrl()}/{id[-1]}",
                                headers=self.getPutHeader(),
                                data=json.dumps(replace_description))

        assert response.status_code == STATUS_CODES.RESPONSE_CODE_ACCEPTED
        response_json = json.loads(response.text)
        assert response_json['content']['description'] == replace_description['description']

        ##########################################################################
        # Confirm that new book exists, and
        # it can be obtained by get request
        ##########################################################################
        book_data['description'] = replace_description['description']
        self.validateData(
            book_id=id[0],
            reference_data=book_data)

    @pytest.mark.timeout(5)
    def test_update_img_url_in_one_book(self):
        book_data = self.getSampleBookData()

        id = self.prepareEnv([book_data], True)

        #####################################################################
        # Update image URL
        #####################################################################
        replace_img_url = {
            "image": {
                "type": "url",
                "data": "http://replaced.com"}}
        response = requests.put(f"{self.endpoint_url}/{self.getApiUrl()}/{id[-1]}",
                                headers=self.getPutHeader(),
                                data=json.dumps(replace_img_url))

        assert response.status_code == STATUS_CODES.RESPONSE_CODE_ACCEPTED
        response_json = json.loads(response.text)

        assert response_json['content']['image']['type'] == replace_img_url['image']['type']
        assert response_json['content']['image']['data'] == replace_img_url['image']['data']

        ##########################################################################
        # Confirm that new book exists, and
        # it can be obtained by get request
        ##########################################################################
        book_data['image'] = replace_img_url['image']
        self.validateData(
            book_id=id[0],
            reference_data=book_data)

    @pytest.mark.timeout(5)
    def test_update_img_base64_in_one_book(self):
        img_type = "base64"
        img_base64 = "aW1n"
        book_data = self.getSampleBookData()
        book_data['image'] = {"type": img_type, "data": img_base64}

        id = self.prepareEnv([book_data], True)

        #####################################################################
        # Update image base64
        #####################################################################
        replace_img_base64 = {
            "image": {
                "type": "base64",
                "data": "cmVwbGFjZWQ"}}

        response = requests.put(f"{self.endpoint_url}/{self.getApiUrl()}/{id[-1]}",
                                headers=self.getPutHeader(),
                                data=json.dumps(replace_img_base64))

        assert response.status_code == STATUS_CODES.RESPONSE_CODE_ACCEPTED
        response_json = json.loads(response.text)

        assert response_json['content']['image']['type'] == replace_img_base64['image']['type']
        assert response_json['content']['image']['data'] == replace_img_base64['image']['data']

        ##########################################################################
        # Confirm that new book exists, and
        # it can be obtained by get request
        ##########################################################################
        book_data['image'] = replace_img_base64['image']
        self.validateData(
            book_id=id[0],
            reference_data=book_data)

    @pytest.mark.book_list_performance
    def test_update_title_in_100_books_sequential(self):
        test_round = 100
        book_data = [self.getSampleBookData() for i in range(0, test_round)]
        id_list = self.prepareEnv(book_data, True)

        def update_one_book():
            ##########################################################################
            # Update book
            ##########################################################################
            id = id_list[-1]
            id_list.pop()

            book_data = {"title": self.get_random_string()}

            assert id != 0
            response = requests.put(
                f"{self.endpoint_url}/{self.getApiUrl()}/{id}",
                headers=self.getPutHeader(),
                data=json.dumps(book_data))
            assert response.status_code == STATUS_CODES.RESPONSE_CODE_ACCEPTED

        self.benchmark.pedantic(update_one_book,  rounds=test_round)

    @pytest.mark.book_list_performance
    def test_update_author_in_100_books_sequential(self):
        test_round = 100
        book_data = [self.getSampleBookData() for i in range(0, test_round)]
        id_list = self.prepareEnv(book_data, True)

        def update_one_book():
            ##########################################################################
            # Update book
            ##########################################################################
            id = id_list[-1]
            id_list.pop()

            book_data = {"author": self.get_random_string()}

            assert id != 0
            response = requests.put(
                f"{self.endpoint_url}/{self.getApiUrl()}/{id}",
                headers=self.getPutHeader(),
                data=json.dumps(book_data))
            assert response.status_code == STATUS_CODES.RESPONSE_CODE_ACCEPTED

        self.benchmark.pedantic(update_one_book,  rounds=test_round)

    @pytest.mark.book_list_performance
    def test_update_year_in_100_books_sequential(self):
        test_round = 100
        book_data = [self.getSampleBookData() for i in range(0, test_round)]
        id_list = self.prepareEnv(book_data, True)

        def update_one_book():
            ##########################################################################
            # Update book
            ##########################################################################
            id = id_list[-1]
            id_list.pop()

            book_data = {"year": str(random.randint(1, 2023))}

            assert id != 0
            response = requests.put(
                f"{self.endpoint_url}/{self.getApiUrl()}/{id}",
                headers=self.getPutHeader(),
                data=json.dumps(book_data))
            assert response.status_code == STATUS_CODES.RESPONSE_CODE_ACCEPTED

        self.benchmark.pedantic(update_one_book,  rounds=test_round)

    @pytest.mark.book_list_performance
    def test_update_description_in_100_books_sequential(self):
        test_round = 100
        book_data = [self.getSampleBookData() for i in range(0, test_round)]
        id_list = self.prepareEnv(book_data, True)

        def update_one_book():
            ##########################################################################
            # Update book
            ##########################################################################
            id = id_list[-1]
            id_list.pop()

            book_data = {"description": self.get_random_string()}

            assert id != 0
            response = requests.put(
                f"{self.endpoint_url}/{self.getApiUrl()}/{id}",
                headers=self.getPutHeader(),
                data=json.dumps(book_data))
            assert response.status_code == STATUS_CODES.RESPONSE_CODE_ACCEPTED

        self.benchmark.pedantic(update_one_book,  rounds=test_round)

    @pytest.mark.book_list_performance
    def test_update_img_url_in_100_books_sequential(self):
        test_round = 100
        book_data = [self.getSampleBookData() for i in range(0, test_round)]
        id_list = self.prepareEnv(book_data, True)

        def update_one_book():
            ##########################################################################
            # Update book
            ##########################################################################
            id = id_list[-1]
            id_list.pop()

            book_data = {
                "image": {
                    "type": "url",
                    "data": "http://" + self.get_random_string()}}

            assert id != 0
            response = requests.put(
                f"{self.endpoint_url}/{self.getApiUrl()}/{id}",
                headers=self.getPutHeader(),
                data=json.dumps(book_data))
            assert response.status_code == STATUS_CODES.RESPONSE_CODE_ACCEPTED

        self.benchmark.pedantic(update_one_book,  rounds=test_round)

    @pytest.mark.book_list_performance
    def test_update_img_base64_in_100_books_sequential(self):
        test_round = 100
        book_data = [self.getSampleBookData() for i in range(0, test_round)]
        id_list = self.prepareEnv(book_data, True)

        def update_one_book():
            ##########################################################################
            # Update book
            ##########################################################################
            id = id_list[-1]
            id_list.pop()
            assert id != 0

            book_data = {
                "image": {
                    "type": "base64",
                    "data": self.get_random_string(100)}}

            response = requests.put(
                f"{self.endpoint_url}/{self.getApiUrl()}/{id}",
                headers=self.getPutHeader(),
                data=json.dumps(book_data))
            assert response.status_code == STATUS_CODES.RESPONSE_CODE_ACCEPTED

        self.benchmark.pedantic(update_one_book,  rounds=test_round)

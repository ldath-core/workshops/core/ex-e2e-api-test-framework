"""

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

"""


from dacite import from_dict
import json
import pytest
import requests
import sys

from tests.list_book_api.test_book_list_common import BookListCommon

sys.path.insert(0, '..')
from support.http_support import STATUS_CODES
from support.book_list_support import Book


@pytest.mark.book_list_api
@pytest.mark.post_book
class BookPost(BookListCommon):

    def validateData(self, book_id: str, reference_data: dict):
        ##########################################################################
        # Confirm that new book exists, and
        # it can be obtained by get request
        ##########################################################################
        response = requests.get(
            f"{self.endpoint_url}/{self.getApiUrl()}/{book_id}",
            headers=self.getHeader())

        assert response.status_code == STATUS_CODES.RESPONSE_CODE_OK
        response_json = json.loads(response.text)
        book = from_dict(data_class=Book, data=response_json['content'])
        assert isinstance(book.id, int)

        assert reference_data['title'] == book.title
        assert reference_data['author'] == book.author
        assert reference_data['year'] == book.year
        if 'description' in reference_data:
            assert reference_data['description'] == book.description
        if 'image' in reference_data:
            if 'type' in reference_data['image'] or 'data' in reference_data['image']:
                assert reference_data['image']['type'] == book.image.type
                assert reference_data['image']['data'] == book.image.data

    @pytest.mark.timeout(5)
    def test_post_one_book_data_with_all_fields(self):
        book_data = self.getSampleBookData()

        self.prepareEnv(None, False)

        ##########################################################################
        # Add new book
        ##########################################################################
        id = self.createBookItem(book_data)

        ##########################################################################
        # Confirm that new book exists, and
        # it can be obtained by get request
        ##########################################################################
        self.validateData(
            book_id=id,
            reference_data=book_data)

    @pytest.mark.timeout(5)
    def test_post_one_book_data_with_required_fields(self):
        book_data = self.getSampleBookData()
        del book_data['description']
        del book_data['image']

        self.prepareEnv(None, False)

        ##########################################################################
        # Add new book
        ##########################################################################
        id = self.createBookItem(book_data)

        ##########################################################################
        # Confirm that new book exists, and
        # it can be obtained by get request
        ##########################################################################
        self.validateData(
            book_id=id,
            reference_data=book_data)

    @pytest.mark.timeout(5)
    def test_post_one_book_data_with_required_fields_and_description(self):
        book_data = self.getSampleBookData()
        del book_data['image']

        self.prepareEnv(None, False)

        ##########################################################################
        # Add new book
        ##########################################################################
        id = self.createBookItem(book_data)

        ##########################################################################
        # Confirm that new book exists, and
        # it can be obtained by get request
        ##########################################################################
        self.validateData(
            book_id=id,
            reference_data=book_data)

    @pytest.mark.timeout(5)
    def test_post_one_book_data_with_required_fields_and_img_url(self):
        book_data = self.getSampleBookData()

        self.prepareEnv(None, False)

        ##########################################################################
        # Add new book
        ##########################################################################
        id = self.createBookItem(book_data)

        ##########################################################################
        # Confirm that new book exists, and
        # it can be obtained by get request
        ##########################################################################
        self.validateData(
            book_id=id,
            reference_data=book_data)

    @pytest.mark.timeout(5)
    def test_post_one_book_data_with_required_fields_and_img_base64(self):
        book_data = self.getSampleBookData()
        book_data["image"] = {"type": "base64", "data": "aW1n"}

        self.prepareEnv(None, False)

        ##########################################################################
        # Add new book
        ##########################################################################
        id = self.createBookItem(book_data)

        ##########################################################################
        # Confirm that new book exists, and
        # it can be obtained by get request
        ##########################################################################
        self.validateData(
            book_id=id,
            reference_data=book_data)

    @pytest.mark.timeout(5)
    def test_post_one_book_data_with_missing_title(self):
        book_data = self.getSampleBookData()
        del book_data['title']

        self.prepareEnv(None, False)

        ##########################################################################
        # Try to add new book
        ##########################################################################
        response = requests.post(f"{self.endpoint_url}/{self.getApiUrl()}",
                                 headers=self.getPostHeader(),
                                 data=json.dumps(book_data))

        assert response.status_code != STATUS_CODES.RESPONSE_CODE_CREATED

    @pytest.mark.timeout(5)
    def test_post_one_book_data_with_missing_author(self):
        book_data = self.getSampleBookData()
        del book_data['author']

        self.prepareEnv(None, False)

        ##########################################################################
        # Try to add new book
        ##########################################################################
        response = requests.post(f"{self.endpoint_url}/{self.getApiUrl()}",
                                 headers=self.getPostHeader(),
                                 data=json.dumps(book_data))

        assert response.status_code != STATUS_CODES.RESPONSE_CODE_CREATED
    
    @pytest.mark.timeout(5)
    def test_post_one_book_data_with_missing_year(self):
        book_data = self.getSampleBookData()
        del book_data['year']

        self.prepareEnv(None, False)

        ##########################################################################
        # Try to add new book
        ##########################################################################
        response = requests.post(f"{self.endpoint_url}/{self.getApiUrl()}",
                                 headers=self.getPostHeader(),
                                 data=json.dumps(book_data))

        assert response.status_code != STATUS_CODES.RESPONSE_CODE_CREATED

    @pytest.mark.book_list_performance
    def test_add_100_books_sequential(self):

        self.prepareEnv(None, False)

        ##########################################################################
        # Implement function which will be under benchmark measurements
        ##########################################################################
        def create_one_book():
            book_data = self.getSampleBookData()

            # Add new book to be fully confident, that requested book will exist
            self.createBookItem(book_data)

        self.benchmark.pedantic(create_one_book, rounds=10)

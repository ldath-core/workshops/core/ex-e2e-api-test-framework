"""

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

"""

import pytest
import os


def pytest_addoption(parser):
    parser.addoption("--host", action="store", help="host url", default=None,
                     required=True)
    parser.addoption("--port", action="store", help="port", default=8080,
                     required=True)
    parser.addoption("--validate-password", action='store_true',
                     dest='validate_password')

@pytest.fixture(scope='class')
def host_and_port(request):
    # This part is called for each test. Only get/set command arguments
    # if the argument is specified in the list of test fixturenames.
    request.cls.endpoint_url = f"{request.config.option.host}:{request.config.option.port}"
    request.cls.validate_password = request.config.option.validate_password

@pytest.fixture(scope='class')
def mock_host_and_port(request):
    # This part is called for each test. Only get/set command arguments
    # if the argument is specified in the list of test fixturenames.
    request.cls.admin_mock_host = os.environ.get('MOCK_ADMINLIST_HOST', 'http://0.0.0.0:8882/')
    request.cls.books_mock_host = os.environ.get('MOCK_BOOKLIST_HOST', 'http://0.0.0.0:8080/')

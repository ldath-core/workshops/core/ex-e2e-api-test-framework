"""

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

"""

import aiohttp
import bcrypt
import json
import pytest
import random
import requests
import string
import sys

from dacite import from_dict
from support.concurrent_request_support import run_concurrent_requests
from support.test_setup_support import get_next_value
from tests.admin_book_api.test_admin_common import AdminBookCommon

sys.path.insert(0,'..')
from support.http_support import STATUS_CODES
from support.admin_account_support import AdminAccount


@pytest.mark.book_admin_api
@pytest.mark.update_admin
class AdminBookUpdate(AdminBookCommon):

    def validateData(self, account_id: str, reference_data: dict):
        #####################################################################
        # Confirm that new account exists and 
        # it can be obtained by get request
        #####################################################################
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}/{account_id}",
                                 headers=self.getHeader())
        
        assert response.status_code == STATUS_CODES.RESPONSE_CODE_OK
        responseJson = json.loads(response.text)
        account = from_dict(data_class=AdminAccount, data=responseJson['content'])
        assert len(account.id) > 0
        assert len(account.passwordHash) > 0

        assert reference_data['email'] == account.email
        assert reference_data['first_name'] == account.firstName  
        assert reference_data['last_name'] == account.lastName
        assert reference_data['version'] == account.version

        if self.validate_password:
            assert bcrypt.checkpw(str.encode(reference_data['password']),
                                  str.encode(account.passwordHash))

    def get_random_string(self, length: int = 5):
        #####################################################################
        # Generate random String
        #####################################################################
        letters = string.ascii_lowercase
        result_str = ''.join(random.choice(letters) for i in range(length))
        return  result_str
    
    def prepare_env_for_performance_test(self, round: int = 100):
        self.cleanEnvironent()
        return self.createExampleUserAccounts(round)

    @pytest.mark.timeout(10)
    def test_update_first_name_in_one_account(self):
        admin_data = self.getSampleAccountData()
        
        id = self.prepareEnv([admin_data], True)
        assert len(id) > 0

        #####################################################################
        # Update Name
        #####################################################################
        replace_name = { "firstName": "Marcus"}
        response = requests.put(f"{self.endpoint_url}/{self.getApiUrl()}/{id[0]}",
                                headers=self.getPutHeader(),
                                data=json.dumps(replace_name))

        assert response.status_code == STATUS_CODES.RESPONSE_CODE_ACCEPTED
        responseJson = json.loads(response.text)
        assert responseJson['content']['firstName'] == replace_name['firstName']
        
        ##########################################################################
        # Confirm that new account exists and 
        # it can be obtained by get request
        ##########################################################################
        self.validateData(account_id=id[0],
                          reference_data={"email": admin_data['email'],
                                          "first_name": replace_name['firstName'],
                                          "last_name": admin_data['lastName'],
                                          "version": 1,
                                          "password": admin_data['password']})

    @pytest.mark.timeout(10)
    def test_update_last_name_in_one_account(self):
        admin_data = self.getSampleAccountData()

        id = self.prepareEnv([admin_data], True)
        assert len(id) > 0 

        #####################################################################
        # Update Name
        #####################################################################
        replace_name = { "lastName": "Graham"}
        response = requests.put(f"{self.endpoint_url}/{self.getApiUrl()}/{id[0]}",
                                headers=self.getPutHeader(),
                                data=json.dumps(replace_name))

        assert response.status_code == STATUS_CODES.RESPONSE_CODE_ACCEPTED, response
        responseJson = json.loads(response.text)
        assert responseJson['content']['lastName'] == replace_name['lastName']
        
        ##########################################################################
        # Confirm that new account exists and 
        # it can be obtained by get request
        ##########################################################################
        self.validateData(account_id=id[0],
                          reference_data={"email": admin_data['email'],
                                          "first_name": admin_data['firstName'],
                                          "last_name": replace_name['lastName'],
                                          "version": 1,
                                          "password": admin_data['password']})

    @pytest.mark.timeout(10)
    def test_update_password_in_one_account(self):
        admin_data = self.getSampleAccountData()
        
        id = self.prepareEnv([admin_data], True)
        assert len(id) > 0

        #####################################################################
        # Update Name
        #####################################################################
        replace_name = { "password": "Szcz3Brze$zyn"}
        response = requests.put(f"{self.endpoint_url}/{self.getApiUrl()}/{id[0]}",
                                headers=self.getPutHeader(),
                                data=json.dumps(replace_name))

        assert response.status_code == STATUS_CODES.RESPONSE_CODE_ACCEPTED
        responseJson = json.loads(response.text)
        
        if self.validate_password:
            assert bcrypt.checkpw(str.encode(replace_name['password']),
                                  str.encode(responseJson['content']['passwordHash']))
        else:
            assert len(responseJson['content']['passwordHash']) > 0

        ##########################################################################
        # Confirm that new account exists and 
        # it can be obtained by get request
        ##########################################################################
        self.validateData(account_id=id[0],
                          reference_data={"email": admin_data['email'],
                                          "first_name": admin_data['firstName'],
                                          "last_name": admin_data['lastName'],
                                          "version": 1,
                                          "password": replace_name['password']})


    @pytest.mark.book_admin_performance
    def test_update_email_in_100_admin_accounts_sequential(self):
        test_round = 100
        id_list = self.prepare_env_for_performance_test(test_round)

        def update_one_account():
            ##########################################################################
            # Update account
            ##########################################################################
            id = id_list[-1]
            id_list.pop()

            admin_data = {"email": f"{self.get_random_string()}@test.com"}

            assert id != 0
            response = requests.put(f"{self.endpoint_url}/{self.getApiUrl()}/{id}",
                                       headers=self.getPutHeader(),
                                       data=json.dumps(admin_data))
            assert response.status_code == STATUS_CODES.RESPONSE_CODE_ACCEPTED

        self.benchmark.pedantic(update_one_account,  rounds=test_round)


    @pytest.mark.book_admin_performance
    def test_update_first_name_in_100_admin_accounts_sequential(self):
        test_round = 100
        id_list = self.prepare_env_for_performance_test(test_round)

        def update_one_account():
            ##########################################################################
            # Update account
            ##########################################################################
            id = id_list[-1]
            id_list.pop()

            admin_data = {"firstName": f"{self.get_random_string()}"}

            assert id != 0
            response = requests.put(f"{self.endpoint_url}/{self.getApiUrl()}/{id}",
                                       headers=self.getPutHeader(),
                                       data=json.dumps(admin_data))
            assert response.status_code == STATUS_CODES.RESPONSE_CODE_ACCEPTED

        self.benchmark.pedantic(update_one_account,  rounds=test_round)

    @pytest.mark.book_admin_performance
    def test_update_last_name_in_100_admin_accounts_sequential(self):
        test_round = 100
        id_list = self.prepare_env_for_performance_test(test_round)

        def update_one_account():
            ##########################################################################
            # Update account
            ##########################################################################
            id = id_list[-1]
            id_list.pop()

            admin_data = {"lastName": f"{self.get_random_string()}"}

            assert id != 0
            response = requests.put(f"{self.endpoint_url}/{self.getApiUrl()}/{id}",
                                    headers=self.getPutHeader(),
                                    data=json.dumps(admin_data))
            assert response.status_code == STATUS_CODES.RESPONSE_CODE_ACCEPTED

        self.benchmark.pedantic(update_one_account,  rounds=test_round)
    
    @pytest.mark.book_admin_performance
    def test_update_password_in_100_admin_accounts_sequential(self):
        test_round = 100
        id_list = self.prepare_env_for_performance_test(test_round)

        def update_one_account():
            ##########################################################################
            # Update account
            ##########################################################################
            id = id_list[-1]
            id_list.pop()

            # Generate password with 8 characters
            admin_data = {"password": "$herl0CkM3&0$"}

            assert id != 0
            response = requests.put(f"{self.endpoint_url}/{self.getApiUrl()}/{id}",
                                    headers=self.getPutHeader(),
                                    data=json.dumps(admin_data))
            assert response.status_code == STATUS_CODES.RESPONSE_CODE_ACCEPTED

        self.benchmark.pedantic(update_one_account,  rounds=test_round)

    @pytest.mark.timeout(360)
    @pytest.mark.book_admin_performance
    def test_update_all_fields_concurrent(self):
        admin_data = self.getSampleAccountData()

        id = self.prepareEnv([admin_data], True)
        assert len(id) > 0

        #####################################################################
        # Update fields function to be passed to run_concurrent_requests
        #####################################################################
        number_generator = get_next_value()
        async def update_all_fields():
            i = next(number_generator)
            updated_fields = {
                "email": f"updated{i}@example.com",
                "password": f"Szcz3Brze$zyn{i}",
                "firstName": f"firstname{i}",
                "lastName": f"lastname{i}",
            }

            async with aiohttp.ClientSession() as session:
                response = await session.put(f"{self.endpoint_url}/{self.getApiUrl()}/{id[0]}",
                                    headers=self.getPutHeader(),
                                    data=json.dumps(updated_fields))

            assert response.status == STATUS_CODES.RESPONSE_CODE_ACCEPTED
            responseJson = json.loads(await response.text())

            if self.validate_password:
                assert bcrypt.checkpw(str.encode(updated_fields['password']),
                                      str.encode(responseJson['content']['passwordHash']))
            else:
                assert len(responseJson['content']['passwordHash']) > 0
            assert updated_fields['email'] == responseJson["content"]["email"]
            assert updated_fields['firstName'] == responseJson["content"]["firstName"]
            assert updated_fields['lastName'] == responseJson["content"]["lastName"]

        self.benchmark.pedantic(run_concurrent_requests(update_all_fields, 30, 100), rounds=10)

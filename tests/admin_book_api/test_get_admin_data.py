"""

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

"""


from support.concurrent_request_support import run_concurrent_requests
from dacite import from_dict
import aiohttp
import json
import pytest
import random
import requests
import re
import sys

from tests.admin_book_api.test_admin_common import AdminBookCommon

sys.path.insert(0,'..')
from support.http_support import STATUS_CODES
from support.admin_account_support import AdminAccount

EMAIL_REGEX = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,7}\b'
VERSION_NUM = 1 # hardcoded in the first state of development


@pytest.mark.book_admin_api
@pytest.mark.get_admin
class AdminBookGet(AdminBookCommon):

    def validateAccountData(self, responseJson:dict, reference_data:dict):
        account = from_dict(data_class=AdminAccount, data=responseJson)
        assert len(account.passwordHash) > 0

        found_account = False
        if (reference_data['email'] in account.email) and\
           (reference_data['firstName'] in account.firstName) and\
           (reference_data['lastName'] in account.lastName):
            found_account = True

        assert found_account, responseJson
        return found_account

    @pytest.mark.timeout(5)
    def test_get_admins_data(self):
        #####################################################################
        # Check inital value of content without defined query
        #####################################################################
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}", headers=self.getHeader())

        self.validateBaseData(response)

    def test_check_admins_data(self):
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}", headers=self.getHeader())
        responseJson = self.validateBaseData(response)

        if responseJson['content']['count'] == 0:    
            #####################################################################
            # Create new account to ensure, that get can withdraw from db 
            # account which should exits
            #####################################################################
            admin_data = self.getSampleAccountData()

            requests.post(f"{self.endpoint_url}/{self.getApiUrl()}",
                          headers=self.getPostHeader(),
                          data=json.dumps(admin_data))
            response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}", headers=self.getHeader())
            responseJson = json.loads(response.text)


        if  responseJson['content']['count'] > 0:
            if responseJson['content']['count'] > 10:
                count = 10
            else:
                count = responseJson['content']['count']

            for index in range(0, count):
                account = from_dict(data_class=AdminAccount, data=responseJson['content']['results'][index])

                assert len(account.id) > 0
                assert len(account.email) > 0
                assert len(account.passwordHash) > 0
                assert len(account.firstName) >= 0
                assert len(account.lastName) >= 0
                assert re.match(EMAIL_REGEX, account.email)

                assert account.version == VERSION_NUM

    @pytest.mark.get_admin_query
    def test_check_admins_data_with_limit_query_set_to_zero_return_one(self):
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?limit=0", headers=self.getHeader())
        assert response.status_code != STATUS_CODES.RESPONSE_CODE_OK

    @pytest.mark.get_admin_query
    def test_check_admins_data_with_limit_query_set_to_3(self):
        limit = 3
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?limit={limit}", headers=self.getHeader())

        responseJson = self.validateBaseData(response,limit=limit)
        assert len(responseJson['content']['results']) >= 0
    
    @pytest.mark.get_admin_query
    def test_check_admins_data_with_skip_query_set_to_zero(self):
        skip = 0
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?skip={skip}", headers=self.getHeader())
        responseJson = self.validateBaseData(response, skip=skip)
        assert len(responseJson['content']['results']) >= 0
    
    @pytest.mark.get_admin_query
    def test_check_admins_data_with_skip_query_set_to_3(self):
        skip = 3
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?skip={skip}", headers=self.getHeader())
        responseJson = self.validateBaseData(response, skip=skip)

        #####################################################################
        # Check wherever the results is not null, as we expect that db is
        # filled with some data
        #####################################################################
        if responseJson['content']['results']:
            assert len(responseJson['content']['results']) >= 0

    @pytest.mark.get_admin_query
    def test_check_admins_data_with_skip_query_set_to_3_and_limit_set_to_0(self):
        skip = 3
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?skip={skip}&limit=0", headers=self.getHeader())
        assert response.status_code != STATUS_CODES.RESPONSE_CODE_OK

        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?limit=0&skip={skip}", headers=self.getHeader())
        assert response.status_code != STATUS_CODES.RESPONSE_CODE_OK

    @pytest.mark.get_admin_query
    def test_check_admins_data_with_skip_query_set_to_3_and_limit_set_to_3(self):
        skip = 3
        limit = 3
        #####################################################################
        # Check wherever the results is not null, as we expect that db is
        # filled with some data
        #####################################################################
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?skip={skip}&limit={limit}", headers=self.getHeader())
        responseJson = self.validateBaseData(response, skip=skip, limit=limit)
        if responseJson['content']['results']:
            assert len(responseJson['content']['results']) >= 0

        #####################################################################
        # Invert Query
        #####################################################################
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?limit={limit}&skip={skip}", headers=self.getHeader())
        responseJson = self.validateBaseData(response, skip=skip, limit=limit)

        #####################################################################
        # Check wherever the results is not null, as we expect that db is
        # filled with some data
        #####################################################################
        if responseJson['content']['results']:
            assert len(responseJson['content']['results']) >= 0

    @pytest.mark.get_admin_query
    def test_check_admins_data_with_skip_and_limit_query_set_to_random_values(self):
        skip = random.randint(0, 100)
        limit = random.randint(1, 100)
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?skip={skip}&limit={limit}", headers=self.getHeader())
        responseJson = self.validateBaseData(response, skip=skip, limit=limit)

        #####################################################################
        # Check wherever the results is not null, as we expect that db is
        # filled with some data
        #####################################################################
        if responseJson['content']['results']:
            assert len(responseJson['content']['results']) >= 0

        #####################################################################
        # Invert Query
        #####################################################################
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?limit={limit}&skip={skip}", headers=self.getHeader())
        responseJson = self.validateBaseData(response, skip=skip, limit=limit)
        
        #####################################################################
        # Check wherever the results is not null, as we expect that db is
        # filled with some data
        #####################################################################
        if responseJson['content']['results']:
            assert len(responseJson['content']['results']) >= 0

    @pytest.mark.get_admin_query
    def test_check_admins_data_basing_on_email_query(self):
        admin_data = self.getSampleAccountData()

        self.prepareEnv([admin_data], True)

        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?email={admin_data['email']}", headers=self.getHeader())
        responseJson = self.validateBaseData(response)

        assert len(responseJson['content']['results']) == 1, responseJson   # to be fixed after discussion

        #####################################################################
        # Confirm that data is available in db
        #####################################################################
        found_account = False
        for index in range(0, responseJson['content']['count']):
            account = from_dict(data_class=AdminAccount, data=responseJson['content']['results'][index])
            assert len(account.id) > 0
            assert len(account.passwordHash) > 0

            if (admin_data['email'] in account.email) and\
               (admin_data['firstName'] in account.firstName) and\
               (admin_data['lastName'] in account.lastName):
                found_account = True
                break

        assert found_account, responseJson['content']['results']
        
    @pytest.mark.get_admin_query
    def test_check_admins_data_basing_on_email_query_with_limit(self):
        limit = 1
        count = 1
        admin_data = self.getSampleAccountData()

        #####################################################################
        # Prepare environment for test
        #####################################################################
        self.prepareEnv([admin_data], True)

        #####################################################################
        # Confirm that data is available in db
        #####################################################################
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?email={admin_data['email']}&limit=1", headers=self.getHeader())

        responseJson = self.validateBaseData(response, limit=limit, count=count)
        assert len(responseJson['content']['results']) == 1  # to be fixed after discussion

        self.validateAccountData(responseJson['content']['results'][0], admin_data)
    
    @pytest.mark.get_admin_query
    def test_check_admins_data_basing_on_email_query_with_limit_set_to_zero(self):
        admin_data = self.getSampleAccountData()

        self.prepareEnv([admin_data], True)

        #####################################################################
        # Confirm that limit set to 0 return error
        #####################################################################
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?email={admin_data['email']}&limit=0", headers=self.getHeader())
        assert response.status_code != STATUS_CODES.RESPONSE_CODE_OK

    @pytest.mark.get_admin_query
    def test_check_admins_data_basing_on_nonexisting_id(self):
        # Add new account to be fully confident, that requested account will exist
        
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}/zumzum", headers=self.getHeader())
        assert response.status_code != STATUS_CODES.RESPONSE_CODE_OK

        responseJson = json.loads(response.text)
        assert isinstance(responseJson['message'], str )
        assert isinstance(responseJson['status'], int )

    @pytest.mark.get_admin_query
    def test_check_admins_data_basing_on_existing_id(self):
        self.cleanEnvironent()
        admin_data = self.getSampleAccountData()

        #####################################################################
        # Prepare test data
        #####################################################################
        id = self.prepareEnv([admin_data], True)
        assert len(id) > 0

        #####################################################################
        # Confirm that data exists in db and is correctly return by get
        #####################################################################
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}/{id[0]}", headers=self.getHeader())
        responseJson = json.loads(response.text)
        assert response.status_code == STATUS_CODES.RESPONSE_CODE_OK

        self.validateAccountData(responseJson['content'], admin_data)

    @pytest.mark.book_admin_performance
    def test_get_100_admin_accounts_sequential(self):

        self.cleanEnvironent()
        self.createExampleUserAccounts(100)

        def get_one_account():

            for i in range(0, 100):
                if i == 0:
                    response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?limit=1", headers=self.getHeader())
                else:
                    response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?skip={i}&limit=1", headers=self.getHeader())
                if self.avoidDeleting():
                    responseJson = self.validateBaseData(response, skip=i, limit=1, count=(100 + 1)) # add one as one account is avoided from deleting
                else:
                    responseJson = self.validateBaseData(response, skip=i, limit=1, count=100)

                assert len(responseJson['content']['results']) == 1  # to be fixed after discussion

        self.benchmark.pedantic(get_one_account, rounds=10)

    @pytest.mark.book_admin_performance
    def test_get_100_admin_accounts_concurrent(self):

        self.cleanEnvironent()
        self.createExampleUserAccounts(100)

        async def get_one_account():
            async with aiohttp.ClientSession() as session:
                response = await session.get(f"{self.endpoint_url}/{self.getApiUrl()}?skip=0&limit=1", headers=self.getHeader())
            assert response.status == STATUS_CODES.RESPONSE_CODE_OK
            responseJson = json.loads(await response.text())

            if self.avoidDeleting():
                expectedAmountOfAccounts =  100 + 1  # add one as one account is avoided from deleting
            else:
                expectedAmountOfAccounts = 100

            self.validateJsonBaseData(responseJson, skip=0, limit=1, count=expectedAmountOfAccounts)
            assert len(responseJson['content']['results']) == 1  # to be fixed after discussion

        self.benchmark.pedantic(run_concurrent_requests(get_one_account, 30, 100), rounds=10)

"""

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

"""

from support.concurrent_request_support import run_concurrent_requests
import aiohttp
import pytest
import requests
import sys

from tests.admin_book_api.test_admin_common import AdminBookCommon

sys.path.insert(0,'..')
from support.http_support import STATUS_CODES
from support.admin_account_support import AdminAccount


@pytest.mark.book_admin_api
@pytest.mark.delete_admin
class AdminBookDelete(AdminBookCommon):

    @pytest.mark.timeout(5)
    def test_delete_one_account(self):
        # Get sample account data
        admin_data = self.getSampleAccountData()
        id = self.prepareEnv([admin_data], True)

        response = requests.delete(f"{self.endpoint_url}/{self.getApiUrl()}/{id[0]}",
                                     headers=self.getDeleteHeader())

        assert response.status_code == STATUS_CODES.RESPONSE_CODE_ACCEPTED

    @pytest.mark.timeout(5)
    def test_delete_non_existing_account(self):

        id ="dumdum"
        # Ensure that account will not exist
        requests.delete(f"{self.endpoint_url}/{self.getApiUrl()}/{id}",
                                     headers=self.getDeleteHeader())

        # Try to delete not existing account
        response = requests.delete(f"{self.endpoint_url}/{self.getApiUrl()}/{id}",
                                     headers=self.getDeleteHeader())

        assert response.status_code != STATUS_CODES.RESPONSE_CODE_ACCEPTED

    @pytest.mark.book_admin_performance
    def test_delete_100_admin_accounts_sequential(self):
        test_round = 100

        self.cleanEnvironent()
        self.id_list = self.createExampleUserAccounts(test_round)

        def delete_one_account():
            # Add new account to be fully confident, that requested account will exist
            id = self.id_list[-1]
            self.id_list.pop()

            assert id != 0
            response = requests.delete(f"{self.endpoint_url}/{self.getApiUrl()}/{id}",
                                       headers=self.getDeleteHeader())
            assert response.status_code == STATUS_CODES.RESPONSE_CODE_ACCEPTED

        self.benchmark.pedantic(delete_one_account,  rounds=test_round)

    @pytest.mark.book_admin_performance
    def test_delete_100_admin_accounts_concurrent(self):
        test_round = 10
        delete_per_test = 10

        self.cleanEnvironent()

        self.id_list = self.createExampleUserAccounts(test_round * delete_per_test)
        async def delete_one_account(id_list):
            # Add new account to be fully confident, that requested account will exist
            id = id_list[-1]
            id_list.pop()

            assert id != 0
            async with aiohttp.ClientSession() as session:
                response = await session.delete(f"{self.endpoint_url}/{self.getApiUrl()}/{id}",
                                        headers=self.getDeleteHeader())
            assert response.status == STATUS_CODES.RESPONSE_CODE_ACCEPTED

        self.benchmark.pedantic(run_concurrent_requests(delete_one_account, 3, delete_per_test, self.id_list),  rounds=test_round)

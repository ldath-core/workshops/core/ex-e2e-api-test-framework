"""

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

"""


from dacite import from_dict
from support.concurrent_request_support import run_concurrent_requests
from support.test_setup_support import get_next_value
import json
import pytest
import requests
import aiohttp
import sys

from tests.admin_book_api.test_admin_common import AdminBookCommon

sys.path.insert(0,'..')
from support.http_support import STATUS_CODES
from support.admin_account_support import AdminAccount


class AdminBookCommonPost(AdminBookCommon):
    def validateData(self, account_id: str, reference_data: dict):
        ##########################################################################
        # Confirm that new account exists and 
        # it can be obtained by get request
        ##########################################################################
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}/{account_id}",
                                 headers=self.getHeader())
        
        assert response.status_code == STATUS_CODES.RESPONSE_CODE_OK
        responseJson = json.loads(response.text)
        account = from_dict(data_class=AdminAccount, data=responseJson['content'])
        assert len(account.id) > 0
        assert len(account.passwordHash) > 0

        assert reference_data['email'] == account.email
        assert reference_data['firstName'] == account.firstName  
        assert reference_data['lastName'] == account.lastName
        assert reference_data['version'] == account.version

    def validateResponseStatus(self, response):
        responseJson = json.loads(response.text)
        assert response.status_code == STATUS_CODES.RESPONSE_CODE_CREATED
        assert len(responseJson['content']['id']) >= 0
        return responseJson

    @pytest.mark.timeout(5)
    def test_post_one_account_data_with_all_fields(self):
        admin_data = self.getSampleAccountData()

        self.prepareEnv([admin_data], False)

        ##########################################################################
        # Add new account
        ##########################################################################
        response = requests.post(f"{self.endpoint_url}/{self.getApiUrl()}",
                                 headers=self.getPostHeader(),
                                 data=json.dumps(admin_data))

        responseJson = self.validateResponseStatus(response)

        ##########################################################################
        # Confirm that new account exists and 
        # it can be obtained by get request
        ##########################################################################
        self.validateData(account_id=responseJson['content']['id'],
                          reference_data=admin_data)

    @pytest.mark.timeout(5)
    def test_post_one_account_data_with_required_fields(self):
        email = "neo@matrix.com"
        admin_data = {"email": f"{email}",
                      "password": "Oz the great and powerful"}
        
        self.prepareEnv([admin_data], False)

        ##########################################################################
        # Add new account
        ##########################################################################
        response = requests.post(f"{self.endpoint_url}/{self.getApiUrl()}",
                                 headers=self.getPostHeader(),
                                 data=json.dumps(admin_data))
        responseJson = self.validateResponseStatus(response)

        ##########################################################################
        # Confirm that new account exists and 
        # it can be obtained by get request
        ##########################################################################
        self.validateData(account_id=responseJson['content']['id'],
                          reference_data={"email": email, "firstName": "",
                                          "lastName": "", "version": 1,
                                          "password": admin_data['password']})

    @pytest.mark.timeout(5)
    def test_post_one_account_data_with_required_fields_and_version(self):
        email = "neo@matrix.com"
        admin_data = {"email": f"{email}",
                      "password": "Oz the great and powerful",
                      "version": 1}

        self.prepareEnv([admin_data], False)

        ##########################################################################
        # Add new account
        ##########################################################################
        response = requests.post(f"{self.endpoint_url}/{self.getApiUrl()}",
                                 headers=self.getPostHeader(),
                                 data=json.dumps(admin_data))
        responseJson = self.validateResponseStatus(response)

        ##########################################################################
        # Confirm that new account exists and 
        # it can be obtained by get request
        ##########################################################################
        self.validateData(account_id=responseJson['content']['id'],
                          reference_data={"email": email, "firstName": "",
                                          "lastName": "", "version": admin_data['version'],
                                          "password": admin_data['password']})

    @pytest.mark.timeout(5)
    def test_post_one_account_data_with_required_fields_and_first_name(self):
        email = "neo@matrix.com"
        first_name = "Thomas"
        admin_data = {"email": f"{email}",
                      "password": "Oz the great and powerful",
                      "firstName": first_name}

        self.prepareEnv([admin_data], False)

        ##########################################################################
        # Add new account
        ##########################################################################
        response = requests.post(f"{self.endpoint_url}/{self.getApiUrl()}",
                                 headers=self.getPostHeader(),
                                 data=json.dumps(admin_data))
        responseJson = self.validateResponseStatus(response)

        ##########################################################################
        # Confirm that new account exists and 
        # it can be obtained by get request
        ##########################################################################
        self.validateData(account_id=responseJson['content']['id'],
                          reference_data={"email": email, "firstName": first_name,
                                          "lastName": "", "version": 1,
                                          "password": admin_data['password']})

    @pytest.mark.timeout(5)
    def test_post_one_account_data_with_required_fields_and_last_name(self):
        email = "neo@matrix.com"
        last_name = "Anderson"
        admin_data = {"email": f"{email}",
                      "password": "Oz the great and powerful",
                      "lastName": last_name}

        self.prepareEnv([admin_data], False)

        ##########################################################################
        # Add new account
        ##########################################################################
        response = requests.post(f"{self.endpoint_url}/{self.getApiUrl()}",
                                 headers=self.getPostHeader(),
                                 data=json.dumps(admin_data))
        responseJson = self.validateResponseStatus(response)

        ##########################################################################
        # Confirm that new account exists and 
        # it can be obtained by get request
        ##########################################################################
        self.validateData(account_id=responseJson['content']['id'],
                          reference_data={"email": email, "firstName": "",
                                          "lastName": last_name, "version": 1,
                                          "password": admin_data['password']})

    @pytest.mark.timeout(5)
    def test_post_one_account_data_with_required_fields_and_first_and_last_name(self):
        admin_data = self.getSampleAccountData()
        del admin_data['version']

        self.prepareEnv([admin_data], False)

        ##########################################################################
        # Add new account
        ##########################################################################
        response = requests.post(f"{self.endpoint_url}/{self.getApiUrl()}",
                                 headers=self.getPostHeader(),
                                 data=json.dumps(admin_data))
        responseJson = json.loads(response.text)
        assert response.status_code == STATUS_CODES.RESPONSE_CODE_CREATED
        assert len(responseJson['content']['id']) >= 0
        id = responseJson['content']['id']

        ##########################################################################
        # Confirm that new account exists and
        # it can be obtained by get request
        ##########################################################################
        admin_data['version'] = 1 # add omitted version
        self.validateData(account_id=id,
                          reference_data=admin_data)

    @pytest.mark.timeout(5)
    def test_post_one_account_data_with_missing_password(self):
        admin_data = self.getSampleAccountData()
        del admin_data['password']

        ##########################################################################
        # Try to add new account
        ##########################################################################
        response = requests.post(f"{self.endpoint_url}/{self.getApiUrl()}",
                                 headers=self.getPostHeader(),
                                 data=json.dumps(admin_data))

        assert response.status_code != STATUS_CODES.RESPONSE_CODE_CREATED

    @pytest.mark.timeout(5)
    def test_post_one_account_data_with_missing_email(self):
        admin_data = self.getSampleAccountData()
        del admin_data['email']

        ##########################################################################
        # Try to add new account
        ##########################################################################
        response = requests.post(f"{self.endpoint_url}/{self.getApiUrl()}",
                                 headers=self.getPostHeader(),
                                 data=json.dumps(admin_data))

        assert response.status_code != STATUS_CODES.RESPONSE_CODE_CREATED
    
    @pytest.mark.timeout(5)
    def test_post_one_account_data_with_missing_email_and_password(self):
        admin_data = self.getSampleAccountData()
        del admin_data['email']
        del admin_data['password']

        ##########################################################################
        # Try to add new account
        ##########################################################################
        response = requests.post(f"{self.endpoint_url}/{self.getApiUrl()}",
                                 headers=self.getPostHeader(),
                                 data=json.dumps(admin_data))

        assert response.status_code != STATUS_CODES.RESPONSE_CODE_CREATED

    @pytest.mark.book_admin_performance
    def test_add_100_admin_accounts_sequential(self):

        self.cleanEnvironent()

        ##########################################################################
        # Implement function which will be under benchmark measurements
        ##########################################################################
        number_generator  = get_next_value()
        def create_one_account():
            email = f"test_{next(number_generator)}@example.com"
            first_name = "Michael"
            last_name = "Knight"
            admin_data = {"email": f"{email}",
                          "password": "zumzum",
                          "firstName": f"{first_name}",
                          "lastName": f"{last_name}"}
            # Add new account to be fully confident, that requested account will exist
            response = requests.post(f"{self.endpoint_url}/{self.getApiUrl()}",
                                    headers=self.getPostHeader(),
                                    data=json.dumps(admin_data))

            assert response.status_code == STATUS_CODES.RESPONSE_CODE_CREATED

        self.benchmark.pedantic(create_one_account, rounds=100)


    @pytest.mark.book_admin_performance
    def test_add_100_admin_accounts_concurrent(self):
        self.cleanEnvironent()

        ##########################################################################
        # Implement function which will be run in concurrent in benchmarks
        ##########################################################################
        number_generator  = get_next_value()
        async def create_one_account():
            email = f"test_{next(number_generator)}@example.com"
            first_name = "Michael"
            last_name = "Knight"
            admin_data = {"email": f"{email}",
                          "password": "zumzum",
                          "firstName": f"{first_name}",
                          "lastName": f"{last_name}"}
            async with aiohttp.ClientSession() as session:
                response = await session.post(f"{self.endpoint_url}/{self.getApiUrl()}",
                                        headers=self.getPostHeader(),
                                        data=json.dumps(admin_data))
            assert response.status == STATUS_CODES.RESPONSE_CODE_CREATED

        self.benchmark.pedantic(run_concurrent_requests(create_one_account, 30, 100), rounds=10)


@pytest.mark.book_admin_api
@pytest.mark.post_admin
class AdminBookPost(AdminBookCommonPost):
    pass

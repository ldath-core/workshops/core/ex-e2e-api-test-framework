"""

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

"""

import json
import requests
import sys
import pytest
import unittest


sys.path.insert(0,'..')
from support.http_support import STATUS_CODES
from support.http_support import HEADER_CONTENT_TYPE
from support.admin_account_support import API_SUFFIX
from support.http_support import STATUS_CODES
from support.admin_account_support import AdminAccount
from dacite import from_dict

@pytest.mark.usefixtures("host_and_port")
class AdminBookCommon(unittest.TestCase):

    @pytest.fixture(autouse=True)
    def setupBenchmark(self, benchmark):
        self.benchmark = benchmark

    def getHeader(self):
        """
        Enable overwrite and defining in child class after inheritance
        """
        return HEADER_CONTENT_TYPE

    def getPostHeader(self):
        """
        Enable overwrite and defining in child class after inheritance
        """
        return self.getHeader()

    def getDeleteHeader(self):
        """
        Enable overwrite and defining in child class after inheritance
        """
        return self.getHeader()

    def getApiUrl(self):
        """
        Enable overwrite and defining in child class after inheritance
        """
        return API_SUFFIX

    def avoidDeleting(self):
        """
        Enable possibility to block deleting defined account.
        In case account is not defined, None value is returned. In other case email address
        """
        return None
    
    def getPutHeader(self):
        """
        Overwrite Put header as the update addmin data it should only be available for authorized users
        """
        return HEADER_CONTENT_TYPE

    def createAccount(self, userData:dict)->int:
        """
        Create account using POST request on backend

        Parameters
        ----------
            self: clsobj
                unittest cls instance object
            admin_data: dict
                dictionary which store information required to create new account
        """
        #####################################################################
        # Create new account
        #####################################################################
        response = requests.post(f"{self.endpoint_url}/{self.getApiUrl()}",
                                 headers=self.getPostHeader(),
                                 data=json.dumps(userData))
        responseJson = json.loads(response.text)
        # Check if response code is correct, in other case fail and show response
        assert response.status_code == STATUS_CODES.RESPONSE_CODE_CREATED, responseJson
        # Check if content len is not none, in other case fail and show response
        assert len(responseJson['content']['id']) >= 0, responseJson

        return responseJson['content']['id']

    def createExampleUserAccounts(self, amount: int):
        """
        Create amount of  example accounts, where:
        - account number is defined in the name of email -> test_{i}@example.com

        Parameters
        ----------
            self: clsobj
                unittest cls instance object
            amount: int
                amount of test accounts to be created
        
        Parameters
        ----------
            list
                list of id of created accounts
        """
        id_list = []
        for i in range(0, amount):
            email = f"test_{i}@example.com"
            first_name = "Michael"
            last_name = "Knight"
            admin_data = {"email": f"{email}",
                          "password": "zumzum",
                          "firstName": f"{first_name}",
                          "lastName": f"{last_name}"}
            id_list.append(self.createAccount(admin_data))
        assert len(id_list) == amount
        return id_list

    def prepareEnv(self, admin_data:list, create_account: bool) -> list:
        """
        Prepare backend environment:
        by creating account if not exists.
        If exists, delete and create from scratch

        Parameters
        ----------
            self: clsobj
                unittest cls instance object
            admin_data: list
                list of admin account in dictionary format which store information required to create new account
            create_account: bool
                True -> create account
                False -> omit creating new account
        
        Returns
        ----------
            list
                list of id of newly created account. In case if create_account is set to False, return empty list
        """

        retValId = []
        #####################################################################
        # Check if account does not exists
        #####################################################################
        for account in admin_data:
            response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?email={account['email']}",
                                    headers=self.getHeader())
            responseJson = json.loads(response.text)

            if response.status_code == STATUS_CODES.RESPONSE_CODE_OK and\
            responseJson['content']['count'] == 1:
                if self.avoidDeleting() == None or f"{self.avoidDeleting()}" != f"{account['email']}":
                    response =requests.delete(f"{self.endpoint_url}/{self.getApiUrl()}/{responseJson['content']['results'][0]['id']}",
                                            headers=self.getDeleteHeader())
                    assert response.status_code == STATUS_CODES.RESPONSE_CODE_ACCEPTED

            if create_account:
                retValId.append(self.createAccount(account))

        return retValId

    def getSampleAccountData(self)->dict:
        """
        Get sample account presentation as dictionary
        """
        email = "neo@matrix.com"
        first_name = "Thomas"
        last_name = "Anderson"
        return {"email": f"{email}",
                "password": "Oz the great and powerful",
                "firstName": f"{first_name}",
                "lastName": f"{last_name}",
                "version": 1}

    def cleanEnvironent(self):
        """
        Clean db from not needed accounts.
        To avoid of deleting required account, please define in class function avoidDeleting() email which should not be deleted
        """
        # Get current status of admin accounts
        response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}", headers=self.getHeader())
        assert response.status_code == STATUS_CODES.RESPONSE_CODE_OK
        responseJson = json.loads(response.text)

        # Collect all admin accounts
        if responseJson['content']['count'] > 0:
            response = requests.get(f"{self.endpoint_url}/{self.getApiUrl()}?limit={responseJson['content']['count']}",
                                    headers=self.getHeader())
            responseJson = json.loads(response.text)
            assert response.status_code == STATUS_CODES.RESPONSE_CODE_OK
            for index in range(0, responseJson['content']['count']):
                account = from_dict(data_class=AdminAccount, data=responseJson['content']['results'][index])
                if self.avoidDeleting() == None or f"{self.avoidDeleting()}" != f"{account.email}":
                    response = requests.delete(f"{self.endpoint_url}/{self.getApiUrl()}/{account.id}",
                                               headers=self.getDeleteHeader())
                    assert response.status_code == STATUS_CODES.RESPONSE_CODE_OK or\
                        response.status_code == STATUS_CODES.RESPONSE_CODE_ACCEPTED

    def validateBaseData(self, response, *, skip:int=0, limit:int=10, count:int=0,
                         status_code:STATUS_CODES=STATUS_CODES.RESPONSE_CODE_OK):
        """
        Validate status code, skip, limit, count values in json
        
        Parameters
        ----------
        responsJson: dict
            presentation of response dictionary base on get API from ex-book
        skip: int
            reference value of the skip value defined for get API from ex-book
        limit: int
            reference value of the limit value defined for get API from ex-book
        count: int
            reference value of the count value defined for get API from ex-book
        status_code: STATUS_CODE
            reference status code to validate response status result
        """
        responseJson = json.loads(response.text)
        assert response.status_code == status_code
        self.validateJsonBaseData(responseJson,skip=skip, limit=limit, count=count)
        return responseJson
    
    def validateJsonBaseData(self, responseJson:dict, *, skip:int=0, limit:int=10, count:int=0):
        """
        Validate skip, limit, count values in json

        Parameters
        ----------
        responsJson: dict
            presentation of response dictionary base on get API from ex-book
        skip: int
            reference value of the skip value defined for get API from ex-book
        limit: int
            reference value of the limit value defined for get API from ex-book
        count: int
            reference value of the count value defined for get API from ex-book

        """
        assert responseJson['content']['skip'] == skip, responseJson
        assert responseJson['content']['limit'] == limit
        assert responseJson['content']['count'] >= count
